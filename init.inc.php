<?php
#########################################################################################
# v8.1		190801	PhD		Création par recopie du code répétitif... 
#########################################################################################
#	Initialisations communes
#########################################################################################

require_once ('../dbaconit/globalvars.php');	// Paramètres DBAconit 
require_once ('./globalvars.php');
require_once ('./globalfcts.php');
require_once ('./include/xml_inc.php');

//date_default_timezone_set('Europe/Paris');	//???

### Tester s'il y a une session DBAconit en cours
if (session_id() == "") session_start ();	
debug (0x10, 'SESSION', $_SESSION);

### identifier les droits administrateurs 
if (isset($_SESSION['statut'])) {
	// clé d'accès aux salles en travaux ou réservées
	$Xvars['f_cle'] = in_array ('aff_galerie', $tab_droits [$_SESSION['statut']]); 
	// droit de rangement des objets
	$Xvars['f_cle_rgt'] = in_array ('rang_galerie', $tab_droits [$_SESSION['statut']]);
} else $Xvars['f_cle'] = $Xvars['f_cle_rgt'] = FALSE;

?>