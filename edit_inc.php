<?php
#########################################################################################
# v8.1		190820	PhD		Création en regroupant fonctions des modules éditeurs
# v9			190909	PhD		Modifié XML_edit_salle et XML_edit_vitrine, ajouté Synchro
# v9.1		210123	PhD		Corrigé synchro pour tenir compte des objets abdents (sortis de la BdD)
# v9.2		210301	PhD		Ajout des fonctions Jscript
#########################################################################################

$jscript = "
//-------------------------------------------------------------
		var modifier = false;
		var r = false;

		function confirm_gest_files (mod) {
			if (modifier) var r = confirm ('Voulez vous abandonner les modifications ?');
			if (!modifier || r) self.location.href='./gest_files.php?mod='+mod;
		}	
	
		function confirm_retour (fgal, nsal) {
			if (modifier) var r = confirm ('Voulez vous abandonner les modifications ?');
			if (!modifier || r) self.location.href='./galerie.php?fgal='+fgal+'&nsal='+nsal;
		}	
//-------------------------------------------------------------
 ";
 
################################################################## Synchro ###
function Synchro ($galerie) {
# Cette fonction syncronise les codes 'museevirtuel' dans la base de données avec le contenu du fichier galerie 
###
	global $dblink, $Xvars;
	
# Table des codes de placement dans la BdD
	OuvrirBDD ($galerie);		// La BdD est toujours celle fixée dans le fichier galerie/parcours.
	
	$T_bdd = array ();
	// Lire toute la base
	$SQLresult = requete ("SELECT idcollection, museevirtuel 
						FROM Collections WHERE (museevirtuel != '') ORDER BY idcollection ASC");
						
	while ($ligne = mysqli_fetch_assoc ($SQLresult)) { 
		$T_bdd[$ligne['idcollection']] = $ligne['museevirtuel'];
	}
	//debug (255, 'T_BDD', $T_bdd);
	
# Table des codes construits à partir du fichier galerie
	$T_gal = array ();
	
	// Balayage des salles de la galerie
	foreach ($galerie->salle as $salle) {
		$nr = (string) $salle['nr'];
		
		// balayage des vitrines
		for ($i=0; $i<=9; $i++) {
			$vitr = 'vitr_'.$i;
			$vitrine =$salle->$vitr;
			$objets	= (string) $vitrine->objets;
			// Éclater la liste d'objets, composer un code de placement et recopier dans la table galerie
			$n =1;
			$t = explode (',', $objets);

			foreach ($t as $id) {
				$id = trim ($id);
				$nplace = ($n<=9) ? $n : '+';
				if ($id>0) {
					$T_gal[$id] = $nr.'-'.$i.'-'.$nplace;
					$n++;
				}			
			}
		}
	}
	ksort ($T_gal);
	//debug (255, 'T_GAL', $T_gal);
	$Xvars['nb_total'] = count($T_gal);

	// Compteurs des  opérations de retouches dans la BdD
	$nb_sup = $nb_add = $nb_mod = 0;

# Objets supprimés : exploration de la table isue de la BdD
	foreach ($T_bdd as $idb=>$code) {
		if (!isset ($T_gal[$idb])) {
			$nb_sup ++;
			debug (1, 'ID _SUP', $idb);
			requete ("UPDATE Collections SET museevirtuel='' WHERE idcollection=$idb");
		}
	}
	$Xvars['nb_sup'] = $nb_sup;

# Objets modifiés : exploration de la table isue du fichier galerie
# NOTER que ce système ne traite pas le cas où un objet est placé 2 fois : la dernière position gagne...
	foreach ($T_gal as $idg=>$code) {
		if (!isset ($T_bdd[$idg])) {
			requete ("UPDATE Collections SET museevirtuel='$code' WHERE idcollection=$idg");					
			if (mysqli_affected_rows ($dblink)) {	
				$nb_add ++;
				debug (1, 'ID_ADD', $idg);
			} 		// Si la requête est ineffective, c'est que l'objet n'est plus dans la BdD
			else debug (1, 'ID_OBJET ABSENT', $idg.' ==> '.$code);
			
		} else {
			if ($code != $T_bdd[$idg]) {
				$nb_mod ++;
			debug (1, 'ID_MOD', $idg);
			requete ("UPDATE Collections SET museevirtuel='$code' WHERE idcollection=$idg");					
			}
		}
	
	}
	
	$Xvars['nb_add'] = $nb_add;
	$Xvars['nb_mod'] = $nb_mod;
}

############################################################# XML_difficult ###
function XML_difficult ($loop, $attr, $evalue = '') {
	if ($loop === null) return;
	global $Xvars;

	if ($loop < count (DIFFICULT)) {
		$Xvars['diff'] = DIFFICULT[$loop];					
		return  'ACT,LOOP';
	} else return  'EXIT' ;
} 

############################################################# XML_edit_etape ###
function XML_edit_etape ($loop, $attr, $evalue = '') {
	if ($loop === null) return;
	global $salle, $Xvars;

//	On utilise le compteur de boucles pour numéroter les vitrines
	$Xvars['nretape'] = $loop;
	$etp = 'etape_'.$loop;
	$panneau = (string) $salle->$etp->panneau;
	$photo_d = $salle->$etp->photo_d; 
	$photo_g = $salle->$etp->photo_g;
	$Xvars['panneau'] = $panneau;
	$BTN_radio_d = 'choix_d_'.$loop;
	$BTN_radio_g = 'choix_g_'.$loop;
	
	//remise à zéro des champs attr_checked, photo_d et photo_g de Xvars
	$Xvars['photo_d'] = '';
	$Xvars['photo_g'] = '';
	$attr_checked_d = '';
  $attr_checked_g = '';
  
	//test de l'attribut de photo_d
	if (!empty($photo_d['img'])) {
		$Xvars['photo_d'] = trim((string) $photo_d['img']);
		$attr_checked_d = 'img';
		
	} else if (!empty($photo_d['media'])) {
		$Xvars['photo_d'] = trim((string) $photo_d['media']);
		$attr_checked_d = 'media';
		
	} else if (!empty($photo_d['fiche'])) {
		$Xvars['photo_d'] = trim((string) $photo_d['fiche']);
		$attr_checked_d = 'fiche';
	}
	$Xvars['attr_checked_d'] = $attr_checked_d; 
	
	//test de l'attribut de photo_g
	if (!empty($photo_g['img'])) {
		$Xvars['photo_g'] = trim((string) $photo_g['img']);
		$attr_checked_g = 'img';
		
	} else if (!empty($photo_g['media'])) {
		$Xvars['photo_g'] = trim((string) $photo_g['media']);
		$attr_checked_g = 'media';
		
	} else if (!empty($photo_g['fiche'])) {
		$Xvars['photo_g'] = trim((string) $photo_g['fiche']);
		$attr_checked_g = 'fiche';
	}
	$Xvars['attr_checked_g'] = $attr_checked_g;
	
	if ($loop <10)	return  'ACT,LOOP';
	 else return 'EXIT';
} 

############################################################# XML_edit_salle ###
function XML_edit_salle ($loop, $attr, $evalue = '') {
	if ($loop === null) return;
	global $Xvars;

	if ($loop>=count ($Xvars['tab_salle']))	return 'EXIT';
		$Xvars['loop'] = $loop;
		$Xvars['sal_nr'] = $Xvars['tab_salle'][$loop]['nr'];
		$Xvars['sal_etat'] = $Xvars['tab_salle'][$loop]['etat'];
		$Xvars['sal_nom'] = $Xvars['tab_salle'][$loop]['nom'];
	return  'ACT,LOOP';
}

############################################################# XML_edit_vitrine ###
function XML_edit_vitrine ($loop, $attr, $evalue = '') {
	if ($loop === null) return;
	global $salle, $Xvars;

//	On utilise le compteur de boucles pour numéroter les vitrines
	$Xvars['nrvitrine'] = $loop;
	
	$vitr = 'vitr_'.$loop;
	$vitrine = $salle->$vitr;
	$Xvars['cartel'] = (string) $vitrine->cartel;
	$Xvars['objets'] = (string) $vitrine->objets;

	if ($loop <10)	return  'ACT,LOOP';
	 else return 'EXIT';
} 

############################################################# XML_etat ###
function XML_etat ($loop, $attr, $evalue = '') {
	if ($loop === null) return;
	global $Xvars;

	if ($loop < count (ETATS)) {
		$Xvars['etat'] = ETATS[$loop];					
		return  'ACT,LOOP';
	} else return  'EXIT' ;
} 

?>