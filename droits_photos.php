<?php
#########################################################################################
# V1.3		20110730	PhD	création 
# v1.3.01	20110804	PhD	corrections
# v6.0 		20140811	PhD	Ajout galerie Histoire et collection
# v6.6.01	20151102	PhD	MàJ HTML5
# v7.1.02	20181009	PhD	Chgt adresse auteur SPIP
#
#########################################################################################
# Page d'information sur les droits depublication des photos ACONIT
# et crédits photos pour les photos utilisées dans les biographies
#########################################################################################
#

require_once ('./include/inc_tete.php');
?>

<div id="hall">
	<h1>Droits de publication des photos des objets de la collection ACONIT</h1>
	
	<p>Les photos tirées de la base de données DBAconit (vignettes en 150px et photos élargies en 600px) sont libres de droits, <br />sous réserve de la mention «&nbsp;Photo ACONIT&nbsp;»</p>
	<h4> Les photos en pleine définition (1500 à 2500px de large) sont disponibles à ACONIT : </h4>
	<ul>
		<li>Veuillez vous adresser au <a href="http://www.aconit.org/spip/spip.php?auteur8" >secrétariat ACONIT</a>&nbsp;;
		<li>Veuillez préciser le nom de l'objet et — si possible — le numéro d'inventaire et numéro de la photo (Ils sont visibles dans la fiche-résumé de l'objet)&nbsp;; </li>
		<li>Veuillez préciser l'usage prévu de cette photo  : publication web, publication imprimée, usage gratuit ou commercial.</li>
	</ul>
	
	<h1>Crédits photos (biographies)</h1>

	<h2>Galerie informatique</h2>	
	<ul>
		<li>Blaise Pascal - Photo Wikimedia Commons - Domaine public 
			</li>
		<li>Wilhelm Leibniz - Photo Wikimedia Commons - Libre de droits 
			</li>
		<li>Marcel Jacob - © Michel Jacob pour ACONIT    
			</li>
		<li>Herman Hollerith - Photo Wikimedia Commons - Domaine public 
			</li>
		<li>François-Henri Raymond - Bulletin ACONIT 15 - Droits réservés  
			</li>
		<li>Lee De Forest - Photo Wikimedia Commons - Domaine public 
			</li>
		<li>Louis Néel - Site www.medarus.org - Droits réservés
			</li>
		<li>Jean Kuntzmann - Photo IMAG - Courtoisie
			</li>
		<li>Alice Recoque - 01 informatique 09 76 - remis par Mme Recoque
			</li>
		<li>Seymour Cray - Site www.orthstar.com - Droits réservés
			</li>
		<li>Bill Joy - Photo Wikimedia Commons - Licence Creative Commons Attribution 2.0 Generic    </li>
		<li>George Stibitz - Site "www.kerryr.net/pioneers/gallery/"  : "This image is believed to be public domain".
			</li>
		<li>François Gernelle - Site www.feb-patrimoine.com - Droits réservés
			</li>
		<li>Steve Wozniak - Photo Wikimedia Commons - Photographe Al Luckow - Utilisation autorisée
			</li>
		<li>Steve Jobs - Photo Wikimedia Commons- Auteur Matt Yohe - licence Creative Commons Paternité
			</li>
		<li>Thomas J. Watson - Photo Wikimedia Commons- Source IBM Corporate Archives - licence Creative Commons Paternité
			</li>
		<li>Hewlett-Packard - Site bisquiz.blogspot.com - Droits réservés
			</li>
		<li>Alain Colmerauer - Photo IMAG - Courtoisie
			</li>
		<li>Nolan Bushnell - Photo Wikimedia Commons- Auteur Javier Candeira - licence Creative Commons Paternité</li>
		<li>Louis Pouzin - Site www.reseaux-telecoms.net - Droits réservés
			</li>
		<li>Ada Lovelace - Photo Wikimedia Commons - Domaine public
			</li>
		<li>Bernard Vauquois - - Photo IMAG - Courtoisie
			</li>
	</ul>

	<h2>Galerie science et techniques</h2>
	<ul>
		<li>Aristide Bergès - Photo Wikimedia Commons - Domaine public 
			</li>
		<li>Pierre-Simon Laplace - Photo Wikimedia Commons - Domaine public 
			</li>
	</ul>

	<h2>Parcours « Histoire et collection informatique »</h2>
	<ul>
		<li>Sauf indication contraire, toutes les photos (hors collection ACONIT)proviennent de Photo Wikimedia Commons et sont du domaine public ou libre de droits.
			</li>
	</ul>

</div>

<!--  Affichage des liens principaux  -->

<div id='piedd'>
	<a href="http://www.aconit.org/spip/" title="Site web ACONIT" target='_blank'>Site web ACONIT</a>
	&nbsp;|&nbsp;
	<a href="../dbaconit/"  title="Bases de données inventaire" target='_blank'>Bases de données inventaire</a>
	&nbsp;|&nbsp;
	<a href="./droits_photos.php"  title="Droits et crédits photos" target='_blank'>Droits et crédits photos</a>
</div>

</body>
</html>