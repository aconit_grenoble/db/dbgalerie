<?php
#########################################################################################
###### RÉCAPITULATION DE TOUTES LES MODIFS DE DBGALERIE
# V1.0		20110307	PhD	création 
# V1.1		20110403	PhD	Ouverture de session
# V1.2		20110705	PhD	Prépublication
# v1.4		20110804	PhD	Ajout date des objets + numéro de version + galerie.conf
# v1.4.01	20110805	PhD	Corrigé espace dans titres dans sal_A
# v1.4.02	20110808	PhD Rebaptisé "Galerie" au singulier
# v2.0		20120521	PhD	Adaptation à DBAconit v10.0 (tables Organismes et Personnes)
# v2.1		20121114	PhD	Ajout Mvt_objet et Mvt_vitrine	
# v3.0		20121211	PhD	Multibases
# v3.0.01	20121224	PhD	Remplacement code 'show' par 'act'
# v3.0.02	20130607	PhD Correction "charset=utf-8" dans inc_tete
# v4.0		20130616	PhD	Refonte avec Xpose et nouveaux modèles hall et credits 
# v4.0.1	20130624	PhD	Ajout de la fonction "Select_db"
# v4.1		20130630	PhD	Ajout des niveaux d'indentation hall, ajout des salles de textes
# v5.0		20130706	PhD	Refonte pour ajout des parcours guidés 
# v5.1		20130708	PhD	Permettre affichage de galeries autres que 0 ou 1
# v5.2		20130713	PhD	Révision tétière, ajout Typo, ajout types photo
# v5.3		20130717	PhD	Révision droit de rangement, paramétré bannière par défaut, légendes photo
# v5.4		20130721	PhD	Réinstallation du panneau "avertissement" dans le hall_P, changé nom dans bannières parcours
# v5.5		20130831	PhD	Reprise du mécanisme d'affichage des photos dans les parcours ajout modele esp_photo
# v5.6		20130923	PhD	Utilisation AdPhoto pour accès nouveau rangt des médias
# v6.0		20140311	PhD	Adaptation au nouvelles tables "médias" de dbaconit_v12
# v6.1		20140604	PhD	Traitement "typo" ajoutés dans esp_P
# v6.2		20150717	PhD Introduction Mysqli
# v6.3		20150815	PhD	Nouvel interface Xpose A8, ajout lien sur les légendes médias
# v6.4		20150827	PhD	Retouches de présentation
# v6.5		20151022	PhD	Ajout lien dans galeries, essai emploi underscore pour nbsp
# v6.6		20151028	PhD Basculé en HTML5, chgt error-reporting, extension typo
# v6.6.01	20151103	PhD	MàJ HTML5
# v6.7		20151114	PhD	Ré-écriture de l'affichage médias esp_photo.xml, photo.php
# v6.7.01	20151124	PhD	Amélioration des positions de blocs, modif photo.php
# v6.7.02	20151125	PhD	Revu l'affichage des fichiers xml, des légendes, et les dimensions de blocs
# v6.8		20151130	PhD	Pas de lien placement dans les salles 'parcours'
# v7			20160224	PhD	Adaptation à V15 DBAconit en UTF-8
# v7.1		20160717	PhD	Ajouté des ancres 'a' sur chaque vitrine (à toutes fins utiles...)
# v7.1.01	20171003	PhD	Changement de dossier du fichier logo
# v8			20190706	NRo	Introduction de l'éditeur XML
# v8.1		20190821	PhD	Ajustements du mode éditeur XML... ajout edit_hall. ATTENTION : PHP v7 imposé
# v8.1.01		190825	PhD		Correction codage caractère PHP 7
# v9				190903	PhD		Inversion du traitement : idcollection dans le fichier galerie, ajout gest_files et synchro
# v9.0.01		200311	PhD		Spécifier le codage base en UTF-8
# v9.1			210122	PhD		Ajout de la fonction "création de fichie XML", ajour 'modele_defaut'
# v9.2			210301	PhD		Ajout protections jscript dans fonctions éditeur et gestion fichier
# v10				220714	PhD		Incorporé dernière version xml_inc (pour PHP 8) - Ajout test $_session 
#####################################################################################################################

$debug = 0x80;								// Cette valeur de 'debug' écrase la valeur fixée dans dbconfig.php. 
															// 1:DebugMSG, 2:requête, 4:Evalstring, 8:Xpose
															// 0x10:$_SESSION, 0x20:$_REQUEST, 0x40:$INPUT, 0x80:error backtrace

$Xvars['vgalerie']  = "v10";	//  No de version

$ban_defaut = 'f_belledonne.jpg';	// bannière par défaut
$logo_defaut = 'l_aconit.png';		// logo par féfaut

$dir_img 					= '../dbgalerie_docs/IMG/';
$dir_img_banniere	= '../dbgalerie_docs/IMG/banniere/'; 
$dir_img_generic	= '../dbgalerie_docs/IMG/generic/';
$dir_sauve				= '../dbgalerie_docs/XML_sauvegardes/';
$dir_textes				= '../dbgalerie_docs/XML_textes/';

#### Table des états, des difficultés, des modèles par défaut
define ('ETATS', ['', 'travaux', 'réserve', 'réservé']);   // (notation PHP 7)
define ('DIFFICULT', ['', 'tous publics', 'jeunes', 'technique', 'expert']);
define ('MODELE_DEFAUT', ['galerie'=>'hall_A', 'parcours'=>'hall_P']);
?>