<?php
#########################################################################################
# V1.0		20110307	PhD		création 
# v5.0		20130706	PhD		Refonte pour ajout des parcours guidés 
# v5.1		20130709	PhD		Correction initialisation f_cle, f_cle_rgt
# v5.2		20130713	PhD		Révision tétière, ajout Typo, 
# v5.3		20130717	PhD		Droit de rangement uniquement sur la base définie par SESSION
#														Paramétré bannière par défaut, Légende photo parcours
# v5.4		20130721	PhD		Réinstallation du panneau "avertissement" dans le hall_P, 
#														changé nom dans bannières parcours
# v5.5		20130831	PhD		Refonte du mécanisme parcours/étapes, ajout méacanisme branche/retour
# v6.2		20150717	PhD 	Introduction Mysqli
# v6.3		20150806	PhD		Nouvel interface Xpose A8
# v6.8		20151130	PhD		Drapeau f_gal dans Xvars		
# v8			20190703	NRo		Adaptation pour l'éditeur XML
# v8.1			190821	PhD		réorganisation des dossiers image, correction niveau
# v8.1.01		190825	PhD		Correction codage caractère PHP 7
# v9				190903	PhD		Refonte : prendre idcollection dans fichier XML, suppresion cdes Mvt
# v9.1			210104	PhD		'f_gal' remplacé par 'mod'
# v10				220714	PhD		Ajout test $_session (erreur pré-existe PHP 8)
#########################################################################################
# "GALERIES ACONIT" 
#
# Le nom "galerie" est appliqué dans ce module indifféremment aux "galeries" de vitrines
# et aux "parcours" guidés par étapes, qui sont de même organisation technique.
# 
#########################################################################################

#-- paramètres standards
# fgal		: nom du fichier galerie XML - controlé par rapport à la liste des fichiers
# nsal		: numéro de la salle recherchée - contrôlé numérique
# fgalr		: galerie de retour si branchement d'un parcours à l'autre
# nsalr		: salle de retour si branchement d'un parcours à un autre
# retour	: demande de retour vers un parcours appelant
# rangt		: si présent : mode rangement (modiication des codes museevrtuel) - test set

#---  Déplacement d'un objet
# idcol		: idcollection de l'objet à modifier - contrôlé numérique
# modif		: demande de modification du code musee virtuel d'un objet - test set
# nouv_mv	: nouveau code museevirtuel	- test NormIN

#--- Déplacement d'une vitrine
# mouvt		: demande de mouvement d'une vitrine - test set
# anc_vitr	: numéro ancienne vitrine -  contrôlé numérique
# nouv_sal_vitr	: nouveau code mmm-nnn salle et vitrine

##################################################################### Partie commune ###

require_once ('./galerie_inc.php');

# Initialisations et ouverture de session
require_once ('./init.inc.php');
# Envoyer l'en-tête HTML (après ouverture de session...)
require_once ('./include/inc_tete.php');

####### Traitement des entrées
	// En cas de branchement, enregistrer la configuration de retour
	$fgalr = NormIN ('fgalr', 'R');
	if ($fgalr != '') {
		// Créer la pile des retours si elle n'existe pas
		if (!isset($_SESSION['pileretour'])) $_SESSION['pileretour'] = array ();
		
		$nsalr = $_GET['nsalr'];
		array_unshift ($_SESSION['pileretour'], array('fgalr' => $fgalr, 'nsalr' => $nsalr));
	}

	// En cas de retour après branchement, dépiler nom de parcours et No de salle
	if (isset ($_GET['retour'])) {
		$tretour = array_shift ($_SESSION['pileretour']);
		$fgal = $tretour['fgalr'];
		$nsal = $tretour['nsalr'];	

	// sinon, cas normal, lire les paramètres dans l'appel
	} else {
		$fgal = NormIN ('fgal', 'R');
		$nsal = @$_REQUEST['nsal'];
	}
	
	// Vérifier que le nom du fichier galerie existe.
	if (!file_exists ($dir_textes.$fgal.'.xml')) $fgal = 'galerie0';	// Par défaut galerie informatique...
	$_SESSION['fgal'] = $Xvars['fgal'] = $fgal;
	// Vérifier la salle demandée
	if (!isset($nsal) || !is_numeric($nsal)) $nsal=0; // Toute erreur ramenée à salle 0
	$_SESSION['nsal'] = $Xvars['nsal'] = $nsal;
	
	// Et éventuellement le parcours de retour (simplement pour affichage)
	$Xvars['fretour'] = @$_SESSION['pileretour'][0]['fgalr'];

###################################################################################### Ouvrir le fichier XML galerie ###
####### Dresser les tableaux des salles utilisés dans les différents cas
#
	$galerie = Xopen ($dir_textes.$fgal.'.xml');
	// debug (255, 'GALERIE', $galerie);
	
	$Xvars['tgalerie']  = date('Y-m-d',filemtime ($dir_textes.$fgal.'.xml')); // Date/time fichier galerie
	$gal_item = $galerie->attributes();
	$Xvars['db'] = $db = (string) $gal_item->db;	// Prendre le No de BdD associé au fichier

	// Attention : Il ne suffit pas d'avoir les droit d'affichage des réserves, 
	// ni les droits de rangement des objets dans la session, 
	// encore faut il être sur la même base de données !
	if (!isset ($_SESSION['db']) OR ($db != $_SESSION['db'])) $Xvars['f_cle'] = $Xvars['f_cle_rgt'] = FALSE;	
	
	// Prendre dans le hall les caractéristiques de l'en tête, 
	// utilisé pour le hall et pour les salles sauf indication contraire
	$hall = $galerie->hall;
	if (isset($hall['banniere'])) $Xvars['hall_ban'] = trim ((string) $hall['banniere']) ;
	else $Xvars['hall_ban'] = $ban_defaut; // tétière par défaut
	// Prendre dans le fichier galerie le nom du hall pour habiller les salles des parcours
	$Xvars['hall_nom'] = (string) $hall->nom;
	
	// Dossiers images
	$Xvars['dir_img'] = $dir_img;
	$Xvars['dir_img_banniere'] = $dir_img_banniere;


	// On crée simultanément un tableau réduit contenant uniquement les No de salle 
	// et un tableau complet contenant numéros, statuts,  noms etc. 
	$tab_sal = array ();
	$tab_salle = array ();
	$f_afficher_salle = 0;
	$n = 0;
	foreach ($galerie->salle as $salle) {
		$tab_sal[] = $nr =(string) $salle['nr'];
		$tab_salle[$n]['nr'] = $nr; 
		$tab_salle[$n]['etat'] = $etat =(string) $salle['etat'];
		if ($nr == $nsal) {						
		// Cette salle peut être affichée si aucun état spécial n'est déclaré, ou toujours pour un administrateur
			if (($etat  == '')  OR $Xvars['f_cle']) $f_afficher_salle = TRUE;
			$modele = trim ((string) $salle['modele']);
			$mod = substr ($modele, 0, 3);		// 'sal' ou 'esp' : salle à vitrines ou espace à étapes
		}
		// Si le niveau d'indentation n'est pas précisé : prendre 1
		$tab_salle[$n]['niveau'] = (!empty ($salle['niveau']) ) ? (string) $salle['niveau'] : 1;
		$tab_salle[$n]['nom'] = (string) $salle->nom;
		$tab_salle[$n]['sujet'] = (string) $salle->sujet;
		$n++;
	}
	
	// Trier les tables dans l'ordre des Numéros de salles
	sort ($tab_sal);
	sort ($tab_salle);

####### Ouvrir la base de données	
	//  connecter MySQL et ouvrir la base
	OuvrirBDD ($galerie);			// La BdD est toujours celle fixée dans le fichier galerie/parcours.
	

	if ($f_afficher_salle) {
####### Si OUI : TRAITER LA SALLE
#======================= Identifier les salles (ouvertes) précédentes et suivantes

		// prendre l'index de la table et tester les salles précédentes et suivantes (on boucle les extrémités)
		$id_sal = array_search ($nsal, $tab_sal);
		$nbr_sal = count ($tab_sal);
	
		for ($n=1; $n<=$nbr_sal; $n++) {
			$id_suiv =  ($id_sal+$n) % $nbr_sal;
			if (($tab_salle[$id_suiv]['etat'] == '')  OR $Xvars['f_cle'])
				break;
		}
		$Xvars['nsal_suiv'] =  $tab_sal[$id_suiv];
	
		for ($n=1; $n<=$nbr_sal; $n++) {
			$id_prec =  ($id_sal+$nbr_sal-$n) % $nbr_sal;
			if (($tab_salle[$id_prec]['etat'] == '')  OR $Xvars['f_cle'])
				break;
		}
		$Xvars['nsal_prec'] =  $tab_sal[$id_prec];

#######
		$Xvars['mod'] = $mod;
		if ($mod == 'sal') {
################################################################################# Affichage salle galerie (vitrines) ###

#======================= Extraire les paramètres de la salle du fichier galerie
			foreach ($galerie->salle as $salle) {
				if ($salle['nr']==$nsal) {
					$Xvars['sal_modele'] = $sal_modele = trim ((string) $salle['modele']);
				
					if (!empty($salle['banniere'])) $Xvars['hall_ban'] = trim ((string) $salle['banniere']) ;
					// sinon même tétière que le hall par défaut

					$Xvars['sal_deco'] = trim((string) $salle['deco']);
					$Xvars['sal_nom'] = (string) $salle->nom;
					$Xvars['sal_bio'] = (string) $salle->bio; 
					$Xvars['sal_biopic'] = trim ((string) $salle->biopic);
					$Xvars['sal_sujet'] = (string) $salle->sujet;
					$Xvars['sal_present'] = (string) $salle->presentation;
						$dr = (string) $salle->droits_photos;
					$Xvars['sal_droits_photos'] = ($dr) ? $dr : 'Photo ACONIT';
				
					for ($i=0; $i<=9; $i++) {
						$vitr = 'vitr_'.$i;
						$vitrine =$salle->$vitr;
						$cartel[$i] = (string) $vitrine->cartel;
						$objets[$i]	= (string) $vitrine->objets;
					}
					$Xvars['cartel'] = $cartel;
					$Xvars['objets'] = $objets;
					break;
				}
			}

#======================= Extraire les fiches recherchées
# (fait dans la fonction XML_case)

#======================= Effectuer la mise en forme à partir du modèle sélectionné
			// Ouvrir le fichier ....
			$sal_xml = Xopen ('./XML_modeles/'.$sal_modele.'.xml');
			Xpose ($sal_xml);
		
		} else {
################################################################################# Affichage espace parcours (étapes) ###
#======================= Extraire les paramètres de la salle du fichier galerie
			foreach ($galerie->salle as $salle) {
			//debug (255, 'SALLE', $salle);
				if ($salle['nr']==$nsal) {
					$Xvars['sal_modele'] = $sal_modele = trim ((string) $salle['modele']);
				
					if (!empty($salle['banniere'])) $Xvars['sal_ban'] = trim ((string) $salle['banniere']) ;
					else $Xvars['sal_ban'] = $Xvars['hall_ban'] ;	// même tétière que le hall par défaut

					$Xvars['sal_deco'] = trim((string) $salle['deco']);
					$Xvars['sal_nom'] = (string) $salle->nom;
					$Xvars['sal_sujet'] = (string) $salle->sujet;
					$Xvars['sal_present'] = (string) $salle->presentation;

					$Xvars['salle'] = $salle;
					break;
				}
			}

#======================= Effectuer la mise en forme à partir du modèle sélectionné
			// Ouvrir le fichier ....
			$sal_xml = Xopen ('./XML_modeles/'.$sal_modele.'.xml');
			Xpose ($sal_xml);

		}
	} else  {	
####### Si NON : Afficher le tableau des salles et les consignes
############################################################################################### Tableau des salles  ###

		$mod = 'hall';
		// Prendre dans le fichier galerie les caractéristiques du hall
		$hall = $galerie->hall;
		$hall_modele = trim((string) $hall['modele']);
		$Xvars['hall_nom'] = (string) $hall->nom;
		$Xvars['hall_sujet'] = (string) $hall->sujet;
		$Xvars['hall_presentation'] = (string) $hall->presentation;
		$Xvars['niveau_court'] = 1;			// Niveau d'ndentation courant

		// Charger le tableau des salles dans le tableau de contexte
		$Xvars['tab_salle'] = $tab_salle;

		// Appliquer le modèle "Hall"
		$hall = Xopen ('./XML_modeles/'.$hall_modele.'.xml');
		Xpose ($hall);
	
	}

####### Affichage pied de page avec appel éditeur éventuel
####################################################################################################### Pied de page ###

	$Xvars['mod'] = $mod;
	
	$pied = Xopen ('./XML_modeles/pied_page.xml');		// Ouverture et contrôle 
	Xpose ($pied);

# Sortie
Fin ();
?>