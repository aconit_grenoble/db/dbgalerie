<?php
#########################################################################################
# v9.1		210123	PhD		Création
#########################################################################################


require_once ('./edit_inc.php');	
# Initialisations et ouverture de session
require_once ('./init.inc.php');	

####### Traitement des entrées
$action = NormIn ('action');
$new_file = NormIn ('new_file');
$new_type = NormIn ('new_type');
	if ($new_type == '') $new_type ='parcours';
$new_nom = NormIn ('new_nom');

$db = $_SESSION['db'];

// En mode création, il n'y a ni fichier, ni salle qui soit défini (important por appeler gestion fichiers)
unset ($_SESSION['fgal'], $_SESSION['nsal']);

#======================= Éxécuter les commandes ===================================
switch ($action) {
	case 'creation':
		// Le contrôle de présence de new_file et de new_nom est fait par JScript ?????
		
		## Exiger un nom de galerie/parcours
		if (empty ($new_nom)) {
			$Xvars['error'] =  '*** Le nom du hall galerie/parcours est obligatoire ! ***';
			break;
		};

		## Exiger un nom de fichier
		if (empty ($new_file)) {
			$Xvars['error'] =  '*** Le nom de fichier est obligatoire ! ***';
			break;
		};

		## Abandonner si un fichier de ce nom existe déjà
		if (is_file($dir_textes.$new_file.'.xml')) {
			$Xvars['error'] =  '*** Ce nom de fichier existe déjà ! ***';
			break;
		};

		## Sinon créer le fichier
		####################################################
	
		$f = fopen ($dir_textes.$new_file.'.xml', "w");
		if (!$f) {
			$Xvars['error'] = "*** Refus ouverture fichier '$new_file.xml' *** ";  
			break;
		}

		$new_ban = $ban_defaut; 										// tétière par défaut
		$new_date = date('Ymd_B');
		$new_modele = MODELE_DEFAUT[$new_type];

		$contenu =
"<?xml version='1.0' encoding='utf-8' standalone='yes'?>
<!--
$new_date
-->

<$new_type db='$db'>

	<hall modele='$new_modele' banniere='$new_ban' etat='travaux'>		
		<nom> $new_nom </nom>
		<sujet>  </sujet>
		<presentation> 	</presentation>
	</hall>

</$new_type>";

		fwrite ($f,$contenu);
		fclose($f);
		
		## Il faut créer le dossier IMG associé
		mkdir ($dir_img.$new_file);

		## ENCHAINER SUR LE MODULE EDITION GALERIE OU PARCOURS
		$_SESSION['fgal'] = $new_file;
		header("Status: 301 Moved Permanently", false, 301);
		header("Location: ./edit_hall.php");
		debug (255, 'RETOUR >>>>>>'); 				// Pour piéger un retour intempestif
		exit ();															// ... par sécurité				
}

#### AFFICHAGE ÉCRAN DE CRÉATION
#########################################################################################


###  Paramètres prédéfinis
$Xvars['dir_img_banniere'] = $dir_img_banniere;
$Xvars['hall_ban'] = $ban_defaut;
$Xvars['logo_defaut'] = $logo_defaut;
$Xvars['nompublic'] = $Bases[$db]['nompublic'];

$new_etat = 'travaux';	 										// en travaux !

### Paramètres à choisir
$Xvars['new_file'] = $new_file;
$Xvars['new_type'] = $new_type;
$new_modele = MODELE_DEFAUT[$new_type];
$Xvars['new_nom'] = $new_nom;
	

	// Divers
$Xvars['mod'] = 'creat';

#======================= Effectuer la mise en forme à partir du modèle sélectionné
# Envoyer l'en-tête HTML (si il y aredirection, celle-ci doit être faite avant tout autre affichage))
require_once ('./include/inc_tete.php');

// Ouvrir le fichier ....
$hall_xml = Xopen ('./XML_modeles/creat_fxml.xml');
Xpose ($hall_xml);


####### Affichage pied de page
####################################################################################################### Pied de page ###

$Xvars['mod'] = '';			// Pas d'appel éditeur en pied de page 

$pied = Xopen ('./XML_modeles/pied_page.xml');		// Ouverture et contrôle 
Xpose ($pied);

# Sortie
Fin ();
?>