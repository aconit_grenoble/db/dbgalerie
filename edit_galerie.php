<?php
#########################################################################################
# V8			190701	NRo		création 
# v8.1		190819	PhD		Ajustements...
# v9			190909	PhD		Compléter, traitement des attributs..., ajouté Synchro
# v9.1		210122	PhD		l'indicateur 'mod' est transmis pour gestion fichier, en plus du pied
#########################################################################################


##################################################################### Partie commune ###

require_once ('./edit_inc.php');	
# Initialisations et ouverture de session
require_once ('./init.inc.php');	
# Envoyer l'en-tête HTML (après ouverture de session...)
require_once ('./include/inc_tete.php');

####### Traitement des entrées
$fgal = $_SESSION['fgal'];
$nsal = $_SESSION['nsal'];
$file = $dir_textes.$fgal.'.xml';

# Toutes les entrées POST sont traitées d'un coup, dans le tableau INPUT
NormINPUT_gal ();

$action = $INPUT['action'] ?? 'annuler';

// Vérifier que le nom du fichier galerie existe.
if (!file_exists ($dir_textes.$fgal.'.xml')) $fgal = 'galerie0';	// Par défaut galerie informatique...
$Xvars['fgal'] = $fgal;
// Vérifier la salle demandée
if (!isset($nsal) || !is_numeric($nsal)) $nsal=0; // Toute erreur ramenée à salle 0
$Xvars['nsal'] = $nsal;
$Xvars['mod'] = 'ed_sal';

###################################################################################### Ouvrir le fichier XML galerie ###
#
$galerie = Xopen ($dir_textes.$fgal.'.xml');
//debug (255, 'GALERIE', $galerie);
	
#======================= Effectuer la modification====================================
switch ($action) {
	case 'annuler':
		break;
	
	case 'enregistrer':
		// Sauvegarde de la version d'origine
		Sauve_xml ($fgal);
		
		foreach ($galerie->salle as $salle) {
			if ($salle['nr']==$nsal) {		// Sélectioner la salle en cours d'édition

				//Modifier attributs
				// Le numéro de salle est traité dans edit_hall
				$salle['modele'] = $INPUT['new_modele'];
				$salle['deco'] = $INPUT['new_deco'];
				$salle['banniere'] = $INPUT['new_ban'];
				$salle['niveau'] = $INPUT['new_niveau'];
				$salle['etat'] = $INPUT['new_etat'];

				// Modifier éléments				
				$salle->nom = $INPUT['new_nom'];
				$salle->sujet = $INPUT['new_sujet'];
				$salle->presentation = $INPUT['new_present'];
				$salle->biopic = $INPUT['new_biopict'];
				$salle->bio = $INPUT['new_bio'];
				
				//Boucle de modification des vitrines
				// NOTE : l'opération d'assignation CRÉE les vitrines inexistantes, sans utilisation de la fonction AddChild...
				// 				Ce n'est indiqué nulle part dans les notices mais fonctionne très correctement.
				$i = 0;
				while ($i < 10) {	
					$vitr = 'vitr_'.$i;
					// Créer la vitrine si elle n'existe pas 
					if (empty ($salle->$vitr)) {
						$salle->addChild($vitr);
					}
					$vitrine = $salle->$vitr;

					$vitrine->cartel = $INPUT['new_cartel'.$i];
					$vitrine->objets = $INPUT['new_objets'.$i];
					$i++;
				}				
				break; 		// La salle est traitée, inutile de chercher une autre salle dans cette boucle
			}	
		}
		
		//enregistrer la MODIFICATION
		$galerie->asXML($file); 
		$Xvars['enreg_OK'] = 'TRUE';
		
# 	Synchroniser les codes 'museevirtuel' dans la BdD avec ce fichier
		Synchro ($galerie);
		
	break;
}
	
################################################################################# Affichage salle galerie (vitrines) ###
# Prendre dans le hall les caractéristiques de l'en tête, 
# utilisé pour le hall et pour les salles sauf indication contraire
$hall = $galerie->hall;
if (!empty($hall['banniere'])) $Xvars['hall_ban'] = trim ((string) $hall['banniere']) ;
else $Xvars['hall_ban'] = $ban_defaut; // tétière par défaut
$Xvars['dir_img_banniere'] = $dir_img_banniere;

####### Puis afficher la salle  

#======================= Extraire les paramètres de la salle du fichier galerie
foreach ($galerie->salle as $salle) {
	if ($salle['nr']==$nsal) {
		// Attributs
		$Xvars['sal_modele'] = trim ((string) $salle['modele']);
		$Xvars['sal_deco'] = trim((string) $salle['deco']);
		$Xvars['sal_ban'] = trim ((string) $salle['banniere']) ;
		// si vide,  même tétière que le hall par défaut
		if (!empty($salle['banniere'])) $Xvars['hall_ban'] = $Xvars['sal_ban'] ;
		$Xvars['sal_niveau'] = trim ((string) $salle['niveau']);	
		$Xvars['sal_etat'] = (string) $salle['etat'];			// va être traité par XML_etat

		//Éléments
		$Xvars['sal_nom'] = (string) $salle->nom;
		$Xvars['sal_bio'] = (string) $salle->bio; 
		$Xvars['sal_biopic'] = trim ((string) $salle->biopic);
		$Xvars['sal_sujet'] = (string) $salle->sujet;
		$Xvars['sal_present'] = (string) $salle->presentation;
			$dr = (string) $salle->droits_photos;
		$Xvars['sal_droits_photos'] = ($dr) ? $dr : 'Photo ACONIT';
	
		break;
	}
}


#======================= Effectuer la mise en forme à partir du modèle sélectionné
// Ouvrir le fichier ....
$gal_xml = Xopen ('./XML_modeles/edit_galerie.xml');
Xpose ($gal_xml);


####### Affichage pied de page
####################################################################################################### Pied de page ###

$Xvars['m_edit_galerie'] = TRUE;
$pied = Xopen ('./XML_modeles/pied_page.xml');		// Ouverture et contrôle 
	Xpose ($pied);

# Sortie
Fin ();
?>