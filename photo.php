<!DOCTYPE html >
<html lang="fr">

<!-- 
v6.7		20151114	PhD		Ré-écriture complète
v6.7.01	20151119	PhD		Chgt nom feuille de style
v6.7.02	20151125	PhD		Supr. feuille style, ajouté l'encodage légende, et gestion fermeture fenêtre
-->

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>ACONIT - Galerie </title>
</head>

<?php

  $file = $_REQUEST['file'];
	$enc_legend = @$_REQUEST['legend'];
	$legende = base64_decode ($enc_legend);

	$ext = strtolower (strrchr ($file, '.'));
	switch ($ext) {
	
case '.jpg':
case '.png':
	echo <<<EOD
<body style="padding:0; margin:0; background-color:grey; " onClick="window.close ()">
	<div style="margin:10px;">
		<img alt='Photo' src='$file' />	
		<p style='font-family:Arial,sans-serif; font-size:14px; font-weight:bold; text-align:center;'> $legende </p>
		<p style='font-family:Arial,sans-serif; font-size:10px; font-style:italic; color:white; text-align:right;'> 
				Cliquez sur l'image pour fermer</p>
	</div>
</body>
</html>
EOD;
	break;

case '.mp4':
case '.mov':
case '.mp3':
	echo <<<EOD
<body style="background-color:grey; " onClick="window.close ()">
	<div style="margin:10px;">
		<video src='$file' autoplay controls > </video>
		<p style='font-family:Arial,sans-serif; font-size:14px; font-weight:bold; text-align:center;'> $legende </p>
		<p style='font-family:Arial,sans-serif; font-size:10px; font-style:italic; color:white; text-align:right;'> 
				Cliquez sur l'image pour fermer</p>
	</div>
</body>
</html>
EOD;
	break;
			
case '.pdf':
	echo <<<EOD
<body style="background-color:grey; " >
	<div style="margin:10px;">
		<embed src='$file' width='800' height='800' alt='pdf'
					pluginspage='http://www.adobe.com/products/acrobat/readstep2.html'>
		<p style='font-family:Arial,sans-serif; font-size:14px; font-weight:bold; text-align:center;'> $legende </p>
	</div>
</body>
</html>
EOD;
}
?>

