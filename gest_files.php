<?php
#########################################################################################
# v9			190903	PhD		Création
# v9.1		210123	PhD		Tri des tableaux, 'f_gal' remplacé par 'mod', ajout affichage des médias
# v9.2		210227	PhD		Corrigé la confirmation avant suppression fichier XML
#########################################################################################

################################################################### ArgJSimage ###
function image ($adfile) {
# Composition de l'argument de la fonction JavaScript image
# $adfile	: adresse du fichier image
# $legende	: légende de l'image
#

	$ext = strtolower (strrchr ($adfile, '.'));
	
# Trouver les dimensions de l'image
	if ($ext=='.jpg' OR $ext=='.png' OR $ext=='.gif') {
		$tdim = getimagesize ($adfile);
		$iw = $tdim[0]; $ih = $tdim[1];
	} elseif ($ext=='.mov' OR $ext=='.mp4') {
		$iw = 660; $ih = 500;										// 480px + hauteur du bandeau de commande
	} elseif ($ext=='.mp3') {
		$iw = 320; $ih = 160;
	} elseif ($ext=='.pdf') {
		$iw = 800; $ih = 800;
	}	

# Ajouter espace pour les marges
  $ih += 40;
  $iw += 20;

# Encoder les chaines de caractères
	$url = urlencode ($adfile);
	
	// composer l'argument javascript
//	return "'photo.php?file=$url',$iw, $ih, '$enc_legend'";		// Argument de la fonction JS
	
		## ENCHAINER SUR LE MODULE EDITION GALERIE OU PARCOURS
		header("Status: 301 Moved Permanently", false, 301);
		header("Location: ./photo.php?file=$url");
		debug (255, 'RETOUR >>>>>>'); 				// Pour piéger un retour intempestif
		exit ();															// ... par sécurité				
}

################################################################## XML_liste_files #####
function XML_liste_files ($loop, $attr, $evalue = '') {
###
	if ($loop === null) return;
	global $Xvars, $debug;
	
	$handle = $attr['handle'];

	// À chaque tour, sortir un nom de fichier
	$file = readdir($handle);

	if (!empty ($file)) {
		if (substr($file,0,1) == '.') return 'LOOP'; 
		$Xvars['file'] =$file;
		return 'ACT,LOOP';
	
	} else 
	sort ($Xvars['file']);
	return 'EXIT';

}



######################################################################################### 
#### Initialisations et ouverture de session
require_once ('./init.inc.php');	
# Envoyer l'en-tête HTML (après ouverture de session...)
require_once ('./include/inc_tete.php');
	
####### Traitement des entrées
if (isset ($_SESSION['fgal'])){
	$fgal = $_SESSION['fgal'];
	$nsal = $_SESSION['nsal'] ?? '';
}

$f_back = NormIn ('mod', 'R'); 												// Indicateur du mode de l'appelant
if ($f_back == '') $f_back = NormIn ('f_back', 'R');	// Si plusieurs opérations successives depuis l'écran fichiers
$Xvars['f_back'] = $f_back;			
$Xvars['mod'] = $mod = 'creat';			// Indicateur du traitement en cours

$select_m = @$_POST['select_m'];
$select_f = @$_POST['select_f'];
$select_xml = @$_POST['select_xml'];

$action = NormIN ('action', 'R') ?? 'annuler'; 
		
####################################################################################### Effectuer les actions
switch ($action) {
	case 'afficher':
		$adfile = AdImg ($select_m[0], $fgal);
		// argument fonction javascript image
		$arg = ArgJSimage ($adfile, "cliquer sur l'image pour fermer la fenêtre");
		echo"<script language='javascript'>";
		echo"image($arg)";
		echo"</script>";

	break;

	case 'rappeler':
		// Sauvegarde de la version d'origine
		Sauve_xml ($fgal);

		// Recréer le nom simple, et copier
		$fname = strstr ( $select_f[0], '_2', TRUE);		// valable jusqu'au 3e millénaire
		$res = copy ($dir_sauve. $select_f[0], $dir_textes.$fname.'.xml');
		break;
	
	case 'supprimer': 
		foreach ($select_f as $file_del) {
			unlink ($dir_sauve.$file_del);
		}	
	break;

	case 'sup_media': 
		foreach ($select_m as $file_del) {
			unlink ($dir_img.$fgal.'/'.$file_del);
		}	
	break;

	case 'sup_xml': 
		foreach ($select_xml as $file_del) {
			unlink ($dir_textes.$file_del);		// Supprimer le fichier
			$f_base = strstr($file_del, '.', true);
			rmdir ($dir_img.$f_base);				// et le dossier image associé
		}	
	break;

	case 'télécharger': 
		//  Vérification du transfert de l'image
		if ($_FILES['fichier']['error'] !=0) {
			$Xvars['error'] =  "Erreur de transfert du fichier (err ".$_FILES['fichier']['error'].")";
			break;   // >>>>>>>>>>>>>>>>>>>>>>>>> Sortie du switch
		}

		// Controles réduits... ----
		$nom_fichier = $_FILES['fichier']['name'];	// Nom du fichier uploadé
		$extension = strtolower (substr (strrchr ($nom_fichier,'.'),1));
		if ($extension == 'jpeg') $extension = 'jpg';

		// Écriture
		move_uploaded_file($_FILES['fichier']['tmp_name'], $dir_img.$fgal.'/'.$nom_fichier);
	break;

}

#########################################################################################
####### Affichage de la page

#### S'il existe un fichier XML galerie sélectionné, l'ouvrir et rendre dans le hall les caractéristiques de l'en tête 
if (isset ($fgal) AND is_file ($dir_textes.$fgal.'.xml')) {
	$galerie = Xopen ($dir_textes.$fgal.'.xml');
	
	$hall = $galerie->hall;
	if (isset($hall['banniere'])) $Xvars['hall_ban'] = trim ((string) $hall['banniere']) ;

	// Affichage
	$Xvars['fgal'] = $fgal;
	$Xvars['nsal'] = $nsal;

}	

	if (!isset ($Xvars['hall_ban'])) $Xvars['hall_ban'] = $ban_defaut; // tétière par défaut
	$Xvars['dir_img_banniere'] = $dir_img_banniere;
	
#======================= Ouvrir les dossiers et transmettre les handle 
# Dossier image, sauf si on est en phase de création d'un nouveau fichier XML
#									ou s'il vient d'être supprimé !
if (isset ($fgal) AND is_dir ($dir_img.$fgal)) $Xvars['handle_m'] = opendir ($dir_img.$fgal);

# Dossier des sauvegardes
$Xvars['handle_f'] = opendir ($dir_sauve);

# Dossier des fichiers XML
$Xvars['handle_c'] = opendir ($dir_textes);

#======================= Effectuer la mise en forme à partir du modèle sélectionné
// Ouvrir le fichier ....
$files_xml = Xopen ('./XML_modeles/gest_files.xml');
Xpose ($files_xml);


####### Affichage pied de page
####################################################################################################### Pied de page ###

$pied = Xopen ('./XML_modeles/pied_page.xml');		// Ouverture et contrôle 
Xpose ($pied);

# Sortie
Fin ();
?>