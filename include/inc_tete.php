<!DOCTYPE html >
<html lang="fr">
<?php
# v1.4.02	20110804	PhD		 Ajout infos version en pied gauche
# v3.0.02	20130607	PhD 	Correction charset=utf-8
# v5.2		20130713	PhD 	Retiré l'affichage tétière
# v6.6		20151028	PhD 	Basculé en HTML5
# v6.6.01	20151102	PhD 	MàJ HTML5 : supprimé attr script language
# v6.7		20151114	PhD		Simplifié fonction js image
# v6.7.01	20151119	PhD		Nouveau nom feuille style
# v6.7.02	20151125	PhD		Sorti le calcul de taille
# v7.00		20160224	PhD		Masqué cette liste de modif en les mettant en PHP
# v8			20190624	NRo		Ajout appel styles éditeur
# v9.2		20210301	PhD		Ajout appel fonctions JS (pour éditeur)
?>

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>ACONIT - Galerie </title>
	<meta name="keywords" content="patrimoine, informatique, ordinateur, Grenoble" />
	<link rel="stylesheet" href="./include/styles_base.css" type="text/css" />
	<link rel="stylesheet" href="./include/styles_galerie.css" type="text/css" />
	<link rel="stylesheet" href="./include/styles_edit.css" type="text/css" />
	<link rel="icon" type="image/png" href="./include/favicon.png" /> 
	
<!-- JAVASCRIPT  -->
	<script type="text/javascript">
		//Supprimer toute mise en cadre !
		if (window != window.top) {top.location.href = location.href;}
		
		function image (fname, ww, wh, legend) {

         if (legend) { 
            fname += '&legend=' + legend;
            wh += 40;
         }
//        if (imagewindow) imagewindow.close ();
         imagewindow = window.open (fname, 'Image', 'height='+(wh)+',width='+(ww));
         imagewindow.focus ();
         return;
    } 

		<!-- Fonctions Javascript propres au traitement en cours -->
			<?php if (isset ($jscript)) echo "$jscript" ?> 
			
	</script>
</head>

<body>
