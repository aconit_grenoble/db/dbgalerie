<?php
####
#	Fonctions de traitement des modèles XML par méthode Xpose (origine Xlate de HBP)
####
#	La numérotation des versions est propre à cet outil, indépendamment du programme utilisateur
#
# V_A1.0	2011-03-07	PhD		Création par reprise et modification 'consulter.inc'
# V_A1.1	2011-04-03	PhD		Compléments
# V_A2.0 	2012-11-04	PhD		Ajout fonction Xopen, ajout traitement erreur eval
#							Rplt  = par # pour evalstring, n'est plus forcé pour Xselect
# V_A3.0	2012-12-24	PhD		Remplacement code "show" par "act"
# V_A3.1	2013-02-25	PhD		Afficher toute la chaine évaluée en cas d'erreur
# V_A4		2013-03-15	PhD		Evalstring : remplacé post-traitement (jamais utilisé) par test sur nom d'attribut
# V_A5		2013-04-02	PhD		Ajouté XML_call, remplacé Xlate par Xpose
# V_A6		2013-08-03	PhD		Supprimé le traitement de schaines vides ???
# V_A7		2014-06-20	PhD		Ajout traitement attribut 'flags' dans la fonction XML_call
# V_A8		2015-07-17	PhD		Permutation des 2 paramètres Xpose, le nom du node est optionnel
# V_A9		2015-11-02	PhD		Correction bogue : récupérer le code Xaction sur les balises de fin
# V_A10		2016-01-11	PhD		Ajouté param xaction dans appel fonctions xml_
# V_A11		2016-02-20	PhD		Ajouté retour ligne devant balise finale
# v_A12		2016-08-02	PhD		Ajouté affichage d'erreurs XML dans Xopen, suprimé saut de ligne devant balise finale
#														Ajouté appel à PostEval dans Xvalue
# v_A13		2016-12-16	PhD		N'exécuter les fonction XML_ que si autorisée par Xselect
# v_A14		2022-07-16	PhD		Remplacé accolades d'index par crochets (PHP 8.1); arg de strpos ne peut être nul (PHP 8.1)
#
#################################################################### Xvalue ###
function Xvalue (
	$b, 			// chaine à évaluer
	$name) 			// Nom du noeud ou de l'attribut en cours de traitement
{
#	Toutes les chaines, valeurs d'attributs et Cdata sont systématiquement 
#	traitées par cette fonction. Si la valeur commence avec un caractère '#'
#	le reste de la chaine est considéré comme une expression PHP et sa
#	valeur est retournée.
#-------------------------------------------------------------------------------
	global $Xvars;				// Tableau des variables
	
	if ($b && ($b[0] == '#')) {
		extract ($Xvars);		// Créer l'espace de noms local

		$st = substr ($b, 1); 
		debug (4, 'XVALUE', "return $st;");
		@trigger_error("");
		$b = eval ("return $st;");
		
		if ($b === FALSE) {
			$error = error_get_last();
			if ($error['type']!= 1024) {
				echo "<pre style='color:red'>[[[ CHAINE : "; print_r ("return $st;"); echo "]]]</pre>";
			}
		}
	}
	
	if (function_exists('PostEval')) $b = PostEval ($b);
	return  $b;
}

###################################################################### Xopen ###
function Xopen ($file) {
#	Ouvre le fichier XML et vérifie sa validité.
# Sans erreurs, la valeur retournée est l'arborescence simplexml,
#	sinon affiche les erreurs et stoppe le traitement
#-------------------------------------------------------------------------------
	libxml_use_internal_errors(true);
	if (false === ($simplexml = @simplexml_load_file($file))) {
		require_once ('inc_tete.php');  // Ouverture retardée de la page HTML 
		Debut ();
		echo "<h2 style='text-align:center; color:red;'>Erreurs dans le fichier &lt;$file&gt; </h2>";
		foreach(libxml_get_errors() as $error) {
			echo "<p> $error->message </p>";
		}
		Fin ();
		exit;						//>>>>>>>>>>>>>>>>>>>>>>>>
  }
	else return $simplexml;
}	

######################################################################## Xtr ###
function Xtr ($nom, $encode = TRUE) {
#	Extrait un champ de la "ligne" de résultat et 
#	par défaut le convertit en utf-8
#-------------------------------------------------------------------------------
	global $ligne;   
	return  ($encode) ? utf8_encode ( $ligne [$nom]) :  $ligne [$nom] ;
}	

################################################################### XML_call ###
function XML_call ($loop, $attr, $Xaction) {
#	Appel d'un nouveau modèle XML
# 	$attr : tableau des attributs du call
# 	- 'file'	Nom du fichier XML à appeler
#	- 'flags'	Liste des drapeaux à positionner puis effacer à la sortie
#	- 'vars'	Liste des variables à recopier de l'ancien contexte Xvars vers le nouveau
#				- si ['vars'] n'est pas défini, on conserve tout le contexte.
#-------------------------------------------------------------------------------

	global $Xvars;
	if ($loop === null) return;		// tag final
	
	if (isset ($attr['vars'])) {	
		// Sauver le contexte actuel et ouvrir un nouveau tableau de variables partagées
		$Xvars_old = $Xvars;
		unset ($Xvars);
	
		// Recopie des variables nécessaires
		$list_vars = explode (',', $attr['vars']);		
		foreach ($list_vars as $vname) $Xvars[$vname] = $Xvars_old[$vname];
	}

	if (isset ($attr['flags'])) {	
		// Positionner les drapeaux
		$list_flags = explode (',', $attr['flags']);		
		foreach ($list_flags as $drapeau) $Xvars[$drapeau] = TRUE;
		unset ($attr['flags']);		// effacer l'attribut traité
	}

	
	// Transmettre les attributs...
	$Xvars = array_merge ($Xvars, $attr); 
//debug (255, 'XVARS', $Xvars);
	// Appel du nouveau fichier XML 	
	$bloc_xml = Xopen ('./XML_modeles/'.$attr['file'].'.xml') ;
	Xpose ($bloc_xml);

	// Restitution de l'ancien contexte
	if (isset ($attr['vars'])) $Xvars = $Xvars_old;	
	// Effacer les drapeaux
	if (isset ($list_flags)) {
		foreach ($list_flags as $drapeau) unset($Xvars[$drapeau]);	
	}

	return 'ACT';
}

##################################################################### Xpose ###
function Xpose (
   $nvalue,			// Contenu XML
   $nname='') 	// Nom du noeud XML à exposer

{
# ATTRIBUTS RÉSERVéS
#	XACTION = Une liste de valeurs :
#		- LOOP : force le bouclage du traitement
#		- PHP  = une chaine de caractères contenant des instructions PHP à exécuter
#		- ACT : ce noeud doit être traité et inclus dans le code résultat
#		- TAG  : cette balise doit être copiée dans le code résultat
#	XSELECT = Une expression dont la valeur VRAIE autorise le traitement du noeud 
#
#-------------------------------------------------------------------------------
	global $Xvars; 			// Tableau des variables, non utilisé ici mais transmis
	if ($nname == '') $nname = $nvalue->getname ();
	debug (8, 'XPOSE_NNAME', $nname);
	
	$Xaction = 'TAG, ACT';	// Actions par défaut pour chaque noeud
	$Xloop = 0;				// Indique qu'on débute le premier cycle !

	// Loop to process multiple instances where required
	while (true) {     
		$attributes = array ();	// Stockage des attributs pour recopie dans flux sortie

		// Read and process attributes	----------------------------------------
		foreach ($nvalue->attributes() as $aname => $value) { 
			$value = trim ($value);  
			
			if (0 == strcasecmp ($aname, 'PHP')) 
				eval ($value);		// Exécution des instructions PHP transmises				
			
			elseif (0 == strcasecmp ($aname, 'XSELECT')) {
									// Si attribut faux, pas d'action sur ce noeud
				$Xaction = Xvalue ($value, $aname) ? $Xaction : '' ;
					
			} elseif (0 == strcasecmp ($aname, 'XACTION') && $Xaction)
				 $Xaction = $value;	// Nouvelle action forcée
				 
			else 	// Pour les autres attributs, copier, évaluer si débute par #
	    		 $attributes[$aname] = ($value[0] == '#') ? Xvalue ($value, $aname) : $value;
		}
		//----------------------------------------------------------------------

		// 1- Si une action est autorisée, exécuter la fonction associée au nom de noeud (si elle existe)
		if (function_exists ('XML_'.$nname) AND $Xaction != '')
			$Xaction = call_user_func ('XML_'.$nname, $Xloop, $attributes, $Xaction);
		
		// (Si demandé) recopier la balise et les attributs
		if ($Xaction !== NULL AND false !== strpos ($Xaction, 'TAG')) {
			echo "\n<$nname";		// Output the tag heading
			foreach ($attributes as $ak => $av) 
				echo " $ak=\"$av\"";
			echo '>';
		}
		
		// 2- Si le noeud doit être affiché, chercher les "feuilles" terminales
		if ($Xaction !== NULL AND false !== strpos ($Xaction, 'ACT')) {
			$childcnt = 0;		// Comptage des branches	
			
			// Process any sub-elements
			foreach ($nvalue->children() as $sname => $value) {
				$childcnt++;
				Xpose ($value, $sname);
			}
		
			// Si "feuille" terminale, évaluer la chaine valeur
			if ($childcnt == 0) echo Xvalue (trim ($nvalue), $nname);
		}
			
		// 3- Tester si le bouclage est demandé, sinon quitter la boucle While
		if ($Xaction !== NULL AND false !== strpos ($Xaction, 'LOOP'))
			$Xloop ++;
		else
			break;
	}
	
	// Rappeler la fonction associée, l'argument NULL indique "traitement final"
	$Xaction = (function_exists ('XML_'.$nname) ?
							call_user_func ('XML_'.$nname, null, $attributes, $Xaction) : $Xaction);
		
	// (Si demandé) insérer la balise finale, sans retour à la ligne
		if ($Xaction !== NULL AND false !== strpos ($Xaction, 'TAG'))
			echo "</$nname>";
		
}
?>