<?php
#########################################################################################
# V8		190704	NRo		création 
# v8.1	190819	PhD		Ajustements... Utilisation unset
# v9		190909	PhD		Ajout valeur f_gal, enreg_OK
# v9.1	210104	PhD		'f_gal' remplacé par 'mod'
#########################################################################################


require_once ('./edit_inc.php');	
# Initialisations et ouverture de session
require_once ('./init.inc.php');	
# Envoyer l'en-tête HTML (après ouverture de session...)
require_once ('./include/inc_tete.php');
	
####### Traitement des entrées
$fgal = $_SESSION['fgal'];
$nsal = $_SESSION['nsal'];
$file = $dir_textes.$fgal.'.xml';

# Toutes les entrées POST sont traitées d'un coup, dans le tableau INPUT
NormINPUT_gal ();

$action = $INPUT['action'] ?? 'annuler'; 

// Vérifier que le nom du fichier galerie existe.
if (!file_exists ($dir_textes.$fgal.'.xml')) $fgal = 'galerie0';	// Par défaut galerie informatique...
$Xvars['fgal'] = $fgal;
// Vérifier la salle demandée
if (!isset($nsal) || !is_numeric($nsal)) $nsal=0; // Toute erreur ramenée à salle 0
$Xvars['nsal'] = $nsal;
$Xvars['mod'] = 'ed_esp';

###################################################################################### Ouvrir le fichier XML galerie ###

$parcours = Xopen ($dir_textes.$fgal.'.xml');
//debug (255, 'PARCOURS', $parcours);
	
#======================= Effectuer la modification====================================
switch ($action) {
	case 'annuler':
		break;
	
	case 'enregistrer':
		// Sauvegarde de la version d'origine
		Sauve_xml ($fgal);
		
		foreach ($parcours->salle as $salle) {
			if ($salle['nr']==$nsal) {		// Sélectioner la salle en cours d'édition
			
				//Modifier attributs
				// Le numéro de salle n'est pas traité 
				$salle['modele'] = $INPUT['new_modele'];
				$salle['deco'] = $INPUT['new_deco'];
				$salle['banniere'] = $INPUT['new_ban'];
				$salle['niveau'] = $INPUT['new_niveau'];
				$salle['etat'] = $INPUT['new_etat'];

				// Modifier éléments				
				$salle->nom = $INPUT['new_nom'];
				$salle->presentation = $INPUT['new_present'];
				
				//Boucle de modification des étapes
				// NOTE : l'opération d'assignation CRÉE les étapes inexistantes, sans utilisation de la fonction AddChild...
				// 				Ce n'est indiqué nulle part dans les notices mais fonctionne très correctement.
				$i = 0;
				while ($i < 10) {
					$etp = 'etape_'.$i;
					
					$ctn_panneau = 'new_panneau'.$i;
					$salle->$etp->panneau = $INPUT[$ctn_panneau];
				
					$ctn_photo_d = 'new_photo_d'.$i;
					$ctn_photo_g = 'new_photo_g'.$i;
					$BTN_radio_d = 'choix_d_'.$i;
					$BTN_radio_g = 'choix_g_'.$i;
					$photo_d = $salle->$etp->photo_d; 
					$photo_g = $salle->$etp->photo_g; 
				
					if (!empty($INPUT[$ctn_photo_d])) {
						switch ($INPUT[$BTN_radio_d]){
							case 'img':
								$photo_d['img'] = $INPUT[$ctn_photo_d];
								unset ($photo_d['media']);
								unset ($photo_d['fiche']);	
								break;
							
							case 'media':
								unset ($photo_d['img']);
								$photo_d['media'] = $INPUT[$ctn_photo_d];
								unset ($photo_d['fiche']);
								break;
					
							case 'fiche':
								unset ($photo_d['img']);
								unset ($photo_d['media']);
								$photo_d['fiche'] = $INPUT[$ctn_photo_d];
						}						
					} else {
					unset ($salle->$etp->photo_d);
					}
								
					if (!empty($INPUT[$ctn_photo_g])) {
						switch ($INPUT[$BTN_radio_g]){
							case 'img':
								$photo_g['img'] = $INPUT[$ctn_photo_g];
								unset ($photo_g['media']);
								unset ($photo_g['fiche']);	
								break;
							
							case 'media':
								unset ($photo_g['img']);
								$photo_g['media'] = $INPUT[$ctn_photo_g];
								unset ($photo_g['fiche']);
								break;
					
							case 'fiche':
								unset ($photo_g['img']);
								unset ($photo_g['media']);
								$photo_g['fiche'] = $INPUT[$ctn_photo_g];
						}					
					} else unset ($salle->$etp->photo_g);
					
					// Enfin changement de numéro d'étape éventuellement
					// $etape = 'vitr_'.$new;
					// Problème complexe - à approfondir !
						
					$i++;
				}
				break; 		// La salle est traitée, inutile de chercher plus loin
			}	
		}
		
		//enregistrer la MODIFICATION
		$parcours->asXML($file); 
		$Xvars['enreg_OK'] = 'TRUE';
		break;
}

################################################################################# Affichage salle parcours (étapes) ###
# Prendre dans le hall les caractéristiques de l'en tête, 
# utilisé pour le hall et pour les salles sauf indication contraire

$hall = $parcours->hall;
if (!empty($hall['banniere'])) $Xvars['hall_ban'] = trim ((string) $hall['banniere']) ;
else $Xvars['hall_ban'] = $ban_defaut; // tétière par défaut
$Xvars['dir_img_banniere'] = $dir_img_banniere;

####### Puis afficher la salle  

#======================= Extraire les paramètres de la salle du fichier galerie
foreach ($parcours->salle as $salle) {
	if ($salle['nr']==$nsal) {
		// Attributs
		$Xvars['sal_modele'] = trim ((string) $salle['modele']);
		$Xvars['sal_deco'] = trim((string) $salle['deco']);
		$Xvars['sal_ban'] = trim ((string) $salle['banniere']) ;
		// si vide,  même tétière que le hall par défaut
		if (!empty($salle['banniere'])) $Xvars['hall_ban'] = $Xvars['sal_ban'] ;
		$Xvars['sal_niveau'] = trim ((string) $salle['niveau']);	
		$Xvars['sal_etat'] = (string) $salle['etat'];			// va être traité par XML_etat

		//Éléments
		$Xvars['sal_nom'] = (string) $salle->nom;
		$Xvars['sal_present'] = (string) $salle->presentation;
			$dr = (string) $salle->droits_photos;
		$Xvars['sal_droits_photos'] = ($dr) ? $dr : 'Photo ACONIT';
	
		break;
	}
} 


#======================= Effectuer la mise en forme à partir du modèle sélectionné
// Ouvrir le fichier ....
$parc_xml = Xopen ('./XML_modeles/edit_parcours.xml');
Xpose ($parc_xml);


####### Affichage pied de page
####################################################################################################### Pied de page ###

$pied = Xopen ('./XML_modeles/pied_page.xml');		// Ouverture et contrôle 
Xpose ($pied);

# Sortie
Fin ();
?>