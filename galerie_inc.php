<?php

# V1.0		2011-03-07	PhD	Création
# v4.0		2013-06-20	PhD	Refonte avec Xpose et nouveaux modèles hall et credits 
# v4.1		20130630	PhD		Ajout des niveaux d'indentation hall, ajout des salles de textes
# v5.1		20130708	PhD		Permettre affichage de galeries autres que 0 ou 1
# v5.5		201300831	PhD		Refonte XML_case_objet, XML_etape, XML_panneau supprimé
# v6.0		20140311	PhD		Adaptation au nouvelles tables "médias" de dbaconit_v12
# v6.3		20150815	PhD		(Retouches de présentation)
# v6.5		20151022	PhD		Ajouté traitement lien dans XML_etape
# v6.6.01	20151101	PhD		Reclassement des fonctions
# v6.7		20151111	PhD		Chgt de noms dans xml_etape, xml_case_objet
# v8.1			190815	PhD		Remplacé isset par !empty, suprimé debugs...
# v9				190903	PhD		Refonte de XML_case et XML_vitrine pour le nouveau mode (références objets dans XML)
###
# Les fonctions XLM_xxx sont appelées par le moteur Xpose
# quand le fichier XML analysé contient une balise XACTION, 
# le nom de la fonction est composé de la chaine 'XML_' 
# suivi du nom du noeud XML traité
################################################################ XML_case ###
function XML_case ($loop, $attr, $evalue = '') {
	if ($loop === null) return;
	global $dblink, $tobj, $Xvars;

	// Au premier tour, mettre en forme la liste des objets
	if ($loop == 0) {
		$nrvitrine = $Xvars['nrvitrine'];
		$lobj = $Xvars['objets'][$nrvitrine];
		debug (1, 'LOBJ', $lobj);		
		// si la liste est vide, sortie immédiate
		if (empty ($lobj)) return 'EXIT';
		else $tobj = explode (',', $lobj);
	}
	
	// si la table est vide, sortie  immédiate
	if (count ($tobj) == 0) return 'EXIT';
	// sinon, lire la base pour l'objet courant
	else {
		$Xvars['idcollection '] = $idcollection = trim (array_shift ($tobj));
		
		$SQLresult = requete ( "SELECT * FROM (Collections)
			left join Machines on Machines.idmachine=Collections.idmachine 
			left join Designations on Designations.iddesignation=Machines.iddesignation    
			left join Documents on Documents.iddocument=Collections.iddocument     
			left join Logiciels on Logiciels.idlogiciel=Collections.idlogiciel
			left join Col_Med on Col_Med.idcollection=Collections.idcollection
			WHERE Collections.idcollection=$idcollection 
			AND mediacle='oui'");
		$ligne =  mysqli_fetch_assoc ($SQLresult);
		// Se protéger contre les objets inscrits mais sans photo !!!
		if (empty ($ligne))		return 'LOOP';  
		
		// On extrait les éléments de cette fiche pour affichage
		$Xvars = array_merge ($Xvars, $ligne);

		// Composer les tables de listes de noms pour cet objet
		$Xvars['tl_orga'] = Compose_tl_orga ($idcollection);
		$Xvars['tl_perso'] = Compose_tl_perso ($idcollection);	

		return 'ACT,LOOP';
	}
} 

################################################################ XML_case_objet ###
function XML_case_objet ($loop, $attr, $evalue = '') {
# Utilisé dans le modèle "esp_photo" appelé par un "call" dans le modèle "esp_P"
#
###
	if ($loop === null) return;
	global $dblink, $Xvars, $debug;

	$idcollection = $Xvars['ref'];
	$SQLresult = mysqli_query ($dblink,  
		"SELECT * FROM Collections
		left join Machines on Machines.idmachine=Collections.idmachine 
		left join Designations on Designations.iddesignation=Machines.iddesignation    
		left join Documents on Documents.iddocument=Collections.iddocument     
		left join Logiciels on Logiciels.idlogiciel=Collections.idlogiciel
		left join Col_Med on Col_Med.idcollection=Collections.idcollection
		LEFT JOIN Medias ON Medias.idmedia=Col_Med.idmedia
		WHERE Collections.idcollection=$idcollection
		AND mediacle='oui'");
	if (! mysqli_num_rows ($SQLresult)) {
		echo "ERREUR - id : $idcollection inconnu";
		return 'EXIT';										// >>> ERROR EXIT
	} else {			// Sinon on extrait les éléments de cette fiche pour affichage
		$ligne =  mysqli_fetch_assoc ($SQLresult);
		$Xvars = array_merge ($Xvars, $ligne);

		// Composer les tables de listes de noms pour cet objet
		$Xvars['tl_orga'] = Compose_tl_orga ($ligne['idcollection']);
		$Xvars['tl_perso'] = Compose_tl_perso ($ligne['idcollection']);	
	
		return 'ACT';
	}

} 

############################################################# XML_etape ###
function XML_etape ($loop, $attr, $evalue = '') {
	if ($loop === null) return;
	global $Xvars;

	$salle = $Xvars['salle'];
	$etape_n = 'etape_'.$loop;
	$etape= $salle->$etape_n;

	//	Traiter les étapes déclarées
	$kret = '';						// code action si travail à exécuter
	if (!empty ($etape)) {
		$kret = 'ACT';
		$Xvars['parite'] = ($loop % 2) ? 'i' : 'p';
		
		$Xvars['panneau'] = (string) $etape->panneau;
		$Xvars['credit'] = (string) $etape->credit_photo;
		
		// Évidemment on pourrait faire une boucle et un tableau photos....
		$ph = $etape->photo_d;
			if (!empty ($ph['img'])) {
				$Xvars['ref_d'] =  trim ((string) $ph['img']);
				$Xvars['mode_d'] = 'img';
			} elseif (!empty ($ph['media'])) {
				$Xvars['ref_d'] =  trim ((string) $ph['media']);
				$Xvars['mode_d'] = 'media';
			} elseif (!empty ($ph['fiche'])) {
				$Xvars['ref_d'] =  trim ((string) $ph['fiche']);
				$Xvars['mode_d'] = 'fiche';
			} else $Xvars['ref_d'] = $Xvars['mode_d'] = '';
			
			$Xvars['plarg_d'] =  (!empty ($ph['largeur'])) ? (string) $ph['largeur'] : 150;
			$Xvars['leg_d'] = trim ((string) $ph);			
		
		$ph = $etape->photo_g;
			if (!empty ($ph['img'])) {
				$Xvars['ref_g'] =  trim ((string) $ph['img']);
				$Xvars['mode_g'] = 'img';
			} elseif (!empty ($ph['media'])) {
				$Xvars['ref_g'] =  trim ((string) $ph['media']);
				$Xvars['mode_g'] = 'media';
			} elseif (!empty ($ph['fiche'])) {
				$Xvars['ref_g'] =  trim ((string) $ph['fiche']);
				$Xvars['mode_g'] = 'fiche';
			} else $Xvars['ref_g'] = $Xvars['mode_g'] = '';
			
			$Xvars['plarg_g'] =   (!empty ($ph['largeur'])) ? (string) $ph['largeur'] : 150;
			$Xvars['leg_g'] = trim ((string) $ph);
		
		$br = $etape->branche;
			$Xvars['br_fgal'] = trim ((string) $br['fgal']);
			$Xvars['br_nsal'] =  trim ((string) $br['nsal']);
		$Xvars['branche'] = (string) $br;
		
		$lien =$etape->lien;
			$Xvars['url'] = trim ((string) $lien['url']);
		$Xvars['lien'] = (string) $lien;	
	}

	if ($loop < 9) return  $kret.',LOOP';
	 else return $kret;
} 

################################################################ XML_salle ###
function XML_salle ($loop, $attr, $evalue = '') {
	if ($loop === null) return;
	global $Xvars;

	$tab_salle = $Xvars['tab_salle'];
	if (array_key_exists ($loop, $tab_salle)) {
		$Xvars['sal_nr'] = $tab_salle[$loop]['nr'];
		$Xvars['sal_etat'] = $tab_salle[$loop]['etat'];
		$Xvars['sal_nom'] = $tab_salle[$loop]['nom'];
		$Xvars['sal_sujet'] =  $tab_salle[$loop]['sujet'];
		
		// Calculer ajout ou retrait de l'indentation : le nouveau niveau devient niveau courant
		$Xvars['var_indent'] = $tab_salle[$loop]['niveau'] - $Xvars['niveau_court'];
		$Xvars['niveau_court'] = $tab_salle[$loop]['niveau'];
	return  'ACT,LOOP';
	} else return 'EXIT';
} 

############################################################# XML_vitrine ###
function XML_vitrine ($loop, $attr, $evalue = '') {
	if ($loop === null) return;
	global $Xvars;

	// Boucler sur les 10 vitrines et transmettre le nrvitrine
	$Xvars['nrvitrine'] = $loop;

	// Sortir après exploration des 10 vitrines
	if ($loop<=9)		{
		if (empty ($Xvars['objets'][$loop])) return 'LOOP';		// Pas d'objets, boucler sans action
		else return  'ACT,LOOP';
		
	} else return 'EXIT';
} 

#####################################################################
?>