<?php
#########################################################################################
# v3.0		20121211	PhD		Création, pour version galeries multibases
# v5.0		20130706	PhD		Refonte pour ajout des parcours guidés 
# v5.1		20130708	PhD		Permettre affichage de galeries autres que 0 ou 1
# v5.2		20130717	PhD		Paramétré bannière par défaut
# v6.3		20150806	PhD		Nouvel interface Xpose A8
# v6.4		20151028	PhD		Forçage error-reporting
# v6.7.02	20151125	PhD		Vérifier extension XML des fichiers	
# v8			20190612	PhD		Correction de commentaires
# v8.1			190806	PhD		Sorti code init.inc, ajustements
# v9.1			210111	PhD		ajouté appel module pied de page standard ; sauter les noms de fichiers sans extension
#########################################################################################
#
# ENTRÉE : ACCUEIL DES VISTEURS, Aiguillage vers les différentes galeries ou la parcours guidés
#
#########################################################################################
#
# Changer le code d'affichage error-reporting pour afficher les "notices"
# Ceci pour permettre le debogage des fichiers XML en ligne (temporaire ?)
error_reporting (E_ALL);	
###	

######################################################################## XML_liste_dir ###
function XML_liste_dir ($loop, $attr, $evalue = '') {

	if ($loop === null) return;
	global $dir_textes, $Xvars;

	//	Premier appel : lister les fichiers du dossier textes
	if ($loop == 0) {
		$Xvars['tab_dir'] = scandir ($dir_textes);
	}

	while ($loop < count ($Xvars['tab_dir'])) {
		$fgal = $Xvars['tab_dir'][$loop];
		if (substr ($fgal, 0, 1) == '.') return 'LOOP';
		$pathinfo = pathinfo ($fgal);
		
		if (!array_key_exists('extension', $pathinfo)) return 'LOOP';	// pas d'extension fichier
		if ($pathinfo['extension'] != 'xml') return 'LOOP';
		
		if ($attr['galerie'] == 'oui')  {
			if (substr ($fgal, 0, 7) != 'galerie') return 'LOOP';
		} else {
			if (substr ($fgal, 0, 7) == 'galerie') return 'LOOP';
		}
		
		$Xvars['fgal'] = $pathinfo['filename'];
		
		
		// Prendre dans chaque fichier galerie les caractéristiques du parcours (affichées dans le hall),
		$galerie = Xopen ($dir_textes.$fgal);		// Ouverture
		$hall = $galerie->hall;
		$Xvars['hall_etat'] =(string) $hall['etat'];
		$Xvars['hall_nom'] = (string) $hall->nom;
		$Xvars['hall_sujet'] = (string) $hall->sujet;
		$Xvars['hall_dif'] = (string) $hall->difficulte;
					
		return  'ACT,LOOP';

	} return 'EXIT';
} 
#########################################################################################

### Initialisations
require_once ('./init.inc.php');

####### Envoyer l'en-tête HTML
require_once ('./include/inc_tete.php');

####### Bannière d'accueil
$Xvars['hall_ban'] = $ban_defaut;
$Xvars['dir_img_banniere'] = $dir_img_banniere;
 
####### Affichage accueil 
$accueil = Xopen ('./XML_modeles/accueil.xml');		// Ouverture et contrôle 
Xpose ($accueil);

####### Affichage pied de page avec appel éditeur éventuel
$Xvars['mod'] = 'creat';	
$pied = Xopen ('./XML_modeles/pied_page.xml');		// Ouverture et contrôle 
Xpose ($pied);

# Sortie
Fin ();
?>