## DBgalerie

## DBgalerie
DBAconit est un package annexe du logiciel de gestion de collection [DBAconit](https://gitlab.com/aconit_grenoble/db/dbaconit) pour permettre la création et la gestion de "galeries muséales virtuelles" 

Exemple des galeries de l'association ACONIT : [Galeries ACONIT](https://db.aconit.org/dbgalerie/).

DBgalerie est programé en français. Les textes utilisateurs sont aux choix du rédacteur...
Les seuls affichages système concernent les fonction d'édition (réservées aux administrateurs) et sont en français.

## Licence CeCILL
Copyright  Association ACONIT
	contributeur : Philippe Denoyelle, (2002-2018) 
	pdenoyelle@aconit.org - info@aconit.org

Ce logiciel est un programme informatique servant à gérer une base de 
données inventaire du patrimoine, en ligne sous MySQL. 
Il s'applique également au logiciel DBAconit et à son annexe DBgalerie

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site [http://www.cecill.info](http://www.cecill.info)

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.

## Documentation
L'ensemble de la documentation se trouve dans le dossier [Documentation](https://db.aconit.org/documentation/),
en particulier :
1. Notice DBgalerie
2. Doc technique DBgalerie

## Crédits
Spécification et réalisation par Ph.Denoyelle,

--------------------
