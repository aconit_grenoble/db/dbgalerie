<?php

# v4.0		20130620	PhD		Créé à partir de galerie_inc
# v5.0		20130706	PhD		Refonte pour ajout des parcours guidés 
# v5.5		20130831	PhD		Retouche un message
# v5.6		20130923	PhD		Ajout AdPhoto
# v6.2		20150717	PhD 	Introduction Mysqli
# v6.3		20150815	PhD		Ajout fonction Col_m
# v6.5		20151022	PhD		Ajout espace insécable dans Typo
# v6.6		20151029	PhD		Compléter Typo avec ;:!?«»
# v6.6.01	20151101	PhD 	MàJ HTML5 : ajout XML_br, XML_hr et XML_img
#	v6.7		20151114	PhD		Remplacé AdPhoto par AdMédia, AdImg et ajouté ArgJSimage
# v6.7.02	20151125	PhD		Revu les tailles des vidéos
# v7.0		20160223	PhD		Suppression fonction Xu, AffNomsXu devient AffNoms
# v8.1		20190820	PhD		Ajout Sauve_xml, NormINPUT, modif Normin_gal, repris fonction Typo
# v9				190902	PhD		Suppression Mvt_objet, Mvt_vitrine, ajout OuvrirBDD
# v9.0.01		200311	PhD		Spécifier le codage base en UTF-8
# v10				220716	PhD		Corrigé functions NormIN pour arg nuls (PHP 8.1)
###


################################################################### AdImg ###
function AdImg ($file, $fgal, $t='') {
# Composition de l'adresse (relative) du fichier media
# $file : nom du fichier média
# $fgal	: nom du fichier galerie en cours de traitement
# $t : 	spécifie vignette 'v' ou pleine image
#

	global $dir_img, $dir_img_generic;	// Bases du nom des dossiers images

# Découper le nom de fichier
	$parts = pathinfo ($file);
	$ext = strtolower ($parts['extension']);
	$name = $parts['filename'];
	
# Commencer par rechercher le média principal (non vignette)
	$adfile = $dir_img.$fgal.'/'.$file;
	
	if (file_exists ($adfile)) {
	// Média principal trouvé, 
		// si la demande porte sur le media principal, plus rien à faire
		// mais si demande de vignette, pour une vidéo ou pdf, rechercher une vraie vignette
		if (($t == 'v') AND ($ext != 'jpg' AND $ext != 'png')) { 
			// si la vignette existe, transmettre l'adresse, sinon prendre une vignette générique
			$adfile = $dir_img.$fgal.'/'.$name.'.jpg';								// vignette jpg
			if (!file_exists ($adfile)) {
				$adfile = $dir_img.$fgal.'/'.$name.'.png';							// vignette png
				if (!file_exists ($adfile))$adfile = $dir_img_generic.'v_'.$ext.'.png';		// vignette générique	
			}
		}	
	
	} else {
	// Si le média principal n'est pas trouvé, on transmet 	un fichier d'image erreur générique
	// comme média principal ou comme vignette
		$adfile = $dir_img_generic.'v_absent.png';		
	}	 

	return $adfile;
}

################################################################### AdMedia ###
function AdMedia ($idmedia, $db, $mode, $t='') {
# Calcul du No de dossier et composition de l'adresse (relative) du fichier media
# $idmedia : index idmedia
# $db =	No de base de données
# $mode : type d'affichage demandé ('media' ou 'fiche')
# $t : 	spécifie vignette 'v' ou rien
#

	global $dir_media, $dir_img_generic;	// Bases du nom des dossiers images
	
	$tab_ext = array ('jpg', 'png', 'pdf', 'mov', 'mp3', 'mp4', 'gif');
	$n = (int)($idmedia / 1000);

# Commencer par rechercher le média principal (non vignette)
	$f_trouv = FALSE;
	// Faire une boucle de recherche sur les différentes extensions connues
	foreach ($tab_ext as $ext) {
		$adfile = $dir_media.'_'.$db.'/'.$ext.'_'.$n.'/'.$idmedia.'.'.$ext;
		if (file_exists ($adfile)) {
			$f_trouv = TRUE;
			break;
		}
	}  

	if ($f_trouv) {
	// Média principal trouvé, 
		// si la demande porte sur le media principal, plus rien à faire
		// rien non plus si demande de vignette pour une photo en mode 'media'
		// mais si demande de vignette, en mode 'fiche' ou avec vidéos, rechercher une vraie vignette
		
		if (($t == 'v') AND ($mode == 'fiche' OR ($ext != 'jpg' AND $ext != 'png'))) { 
			// si la vignette existe, transmettre l'adresse, sinon prendre une vignette générique
			$adfile = $dir_media.'_'.$db.'/'.$ext.'_v'.$n.'/'.$idmedia.'.jpg';							// vignette jpg
			if (!file_exists ($adfile)) {
				$adfile = $dir_media.'_'.$db.'/'.$ext.'_v'.$n.'/'.$idmedia.'.png';						// vignette png
				if (!file_exists ($adfile))$adfile = $dir_img_generic.'v_'.$ext.'.png';				// vignette générique	
			}
		}	
	
	} else {
	// Si le média principal n'est pas trouvé, on transmet 	un fichier d'image erreur générique
	// comme média principal ou comme vignette
		$adfile = $dir_img_generic.'v_absent.png';		
	}	 

	return $adfile;
}

#================================================================= Affnoms ===
function AffNoms ($op, $relation) {
// Sélectionne une liste de noms pour affichage, en fonction de la relation chosie.
// $op : précise si on sélectione 'O'=organisms, 'P'=Personnes ou l'ensemble

	global $Xvars;
	$tl_orga = $Xvars['tl_orga'];
	$tl_perso = $Xvars['tl_perso'];
	
	if (strstr ($op, 'o') && isset ($tl_orga[$relation])) 
		$liste = $tl_orga[$relation];
	
	if (strstr ($op, 'p') && isset ($tl_perso[$relation])) {
		if (!isset($liste)) $liste = $tl_perso[$relation];
		else $liste .= ', '.$tl_perso[$relation];
	}
	return (isset ($liste)) ? $liste : '&nbsp;'; // espace insécable pour éviter case de tableau vide ...
}

################################################################### ArgJSimage ###
function ArgJSimage ($adfile, $legende='') {
# Composition de l'argument de la fonction JavaScript image
# $adfile	: adresse du fichier image
# $legende	: légende de l'image
#

	$ext = strtolower (strrchr ($adfile, '.'));
	
# Trouver les dimensions de l'image
	if ($ext=='.jpg' OR $ext=='.png' OR $ext=='.gif') {
		$tdim = getimagesize ($adfile);
		$iw = $tdim[0]; $ih = $tdim[1];
	} elseif ($ext=='.mov' OR $ext=='.mp4') {
		$iw = 660; $ih = 500;										// 480px + hauteur du bandeau de commande
	} elseif ($ext=='.mp3') {
		$iw = 320; $ih = 160;
	} elseif ($ext=='.pdf') {
		$iw = 800; $ih = 800;
	}	

# Ajouter espace pour les marges
  $ih += 40;
  $iw += 20;

# Encoder les chaines de caractères
	$url = urlencode ($adfile);
	$enc_legend = base64_encode ($legende);
	
	// composer l'argument javascript
	return "'photo.php?file=$url',$iw, $ih, '$enc_legend'";		// Argument de la fonction JS
}

################################################################### Col_m ###
function Col_m ($idmedia) {
# Trouver le No idcollection à partir de la référence idmedia

	$SQLresult = requete ("SELECT idcollection FROM Col_Med WHERE idmedia=$idmedia");
	if (! mysqli_num_rows ($SQLresult)) {
//	echo "ERREUR - référence média $idmedia inconnu";  // erreur non affichée : traité par affichage vignette "absent"
		return '';										// >>> ERROR EXIT
	} else {			
		$ligne =  mysqli_fetch_assoc ($SQLresult);			
		return  $ligne['idcollection'];
	}
}			

#=========================================================== Compose_tl_orga ===
function Compose_tl_orga ($idcollection) {
// Compose un tableau des listes de noms d'organismes, classées par relation

	$tl_orga = array ();
	
	$SQLresult_orga = requete ("SELECT onom, relation FROM Organismes, Col_Org, Relations
		WHERE Organismes.idorganisme=Col_Org.idorganisme AND Relations.idrelation=Col_Org.idrelation
		AND Col_Org.idcollection= $idcollection");
	
   if (0 == (mysqli_num_rows ($SQLresult_orga)))
      return $tl_orga;		// >>>>>>>>>>>>>>>
   
   // Si il y a des résultats
	while ($ligne =  mysqli_fetch_assoc ($SQLresult_orga)) {
		if (!isset ($tl_orga[$ligne['relation']])) 
			$tl_orga[$ligne['relation']] = $ligne['onom'];
		else $tl_orga[$ligne['relation']] .= ", ".$ligne['onom'];
	}
    return $tl_orga;
}
    
#========================================================== Compose_tl_perso ===
function Compose_tl_perso ($idcollection) {
// Compose un tableau des listes de noms de personnes, classées par relation

	global $tl_perso;
	$tl_perso = array ();
	
	$SQLresult_perso = requete ("SELECT pnom, pprenom, relation FROM Personnes, Col_Per, Relations
		WHERE Personnes.idpersonne=Col_Per.idpersonne AND Relations.idrelation=Col_Per.idrelation
		AND Col_Per.idcollection= $idcollection");
	
   if (0 == (mysqli_num_rows ($SQLresult_perso)))
      return $tl_perso;		// >>>>>>>>>>>>>>>
   
   // Si il y a des résultats
	while ($ligne =  mysqli_fetch_assoc ($SQLresult_perso)) {
		if (!isset ($tl_perso[$ligne['relation']])) 
			$tl_perso[$ligne['relation']] = $ligne['pprenom'].' '.$ligne['pnom'];
		else $tl_perso[$ligne['relation']] .= ", ".$ligne['pprenom'].' '.$ligne['pnom'];
	}
	return $tl_perso;
}

###################################################################### debug ###
function debug ($code, $msg, $var="", $mix="") {
// Affiche un message et (éventuellement) le contenu d'une variable 
// si un bit positionné de la  variable $debug correspond à un bit de $code.
// Traite les variables simples et les tableaux.
// Si $mix est non-vide, l'impression est entrecoupée de "." pour éviter un traitement HTML
	global $debug;
	if ($debug & $code) {
		echo "<pre>[[[".$msg ;
		if ($var) {
			echo " : ";
			if (is_array ($var) || is_numeric ($var) || empty ($mix) ) {			
				print_r ($var); 
			} else  {
				for ($i=0; $i<strlen($var); $i+=1) {
					echo ".".substr ($var, $i, 1);
				}
			}			
		} 
		echo "]]]</pre>";
	}
	return;
}

################################################################### fdate ###
function fdate($date1, $idperiode) {
 
 	// Si la date de fabrication (ou de début...) est définie, prendre l'année.
	if ($date1 && ($date1 != '0000-00-00')) return substr ($date1, 0, 4);
	
	// sinon, utiliser la période...
	$tabperiode = array ('', '1750-1775', '1775-1800', '1800-1825', '1825-1850', '1850-1875', '1875-1900', '1900-1925', '1925-1950', '1950-1975', '1975-2000', '2000-2025', '2025-2050');
	return $tabperiode [$idperiode-1];
}

###################################################################### Fin ###
function Fin() {
	echo "\n</body>\n</html>";
}

################################################################## NormIN ###
function NormIN ($nom, $mode="P") {
# Normalisation des textes à l'entrée : protection contre les scripts
# et suppression des espaces tête et queue

	switch ($mode) {
		case "P" : $in = @$_POST[$nom]; break;
		case "G" : $in = @$_GET[$nom]; break;
		case "R" : $in = @$_REQUEST[$nom]; 
	}
	
	// (l'encodage utf-8 - permet de ne pas avoir un retour à blanc avec les versions récentes de php)
	if ($in === NULL) return '';
	else return htmlspecialchars (trim ($in), ENT_QUOTES, 'utf-8');
}

#================================================================= NormIN_gal ===
function NormIN_gal ($nom, $mode="P") {
# Normalisation des textes à l'entrée : protection contre les scripts
# Adaptation au traitement pour les galeries XML : sans traitement des simples et doubles quotes et suppression des \r

	switch ($mode) {
		case "P" : $in = @$_POST[$nom]; break;
		case "G" : $in = @$_GET[$nom]; break;
		case "R" : $in = @$_REQUEST[$nom]; 
	}
	if (!is_array ($in)) { 
		$str = htmlspecialchars ($in, ENT_NOQUOTES);		// sans traitement des quotes simples et doubles
	 	$str = str_replace (array( "\r"), array (''), $str);  
 	} else {
 		foreach ($in as $k=>$item) {
			$str[$k] = htmlspecialchars ($item, ENT_NOQUOTES);		// sans traitement des quotes simples et doubles
 		}
 	}

	return $str;
}

#=============================================================== NormINPUT ===
function NormINPUT_gal () {
# Normalisation NormIN de l'ensemble des résultats d'un formulaire
	global $INPUT;
	foreach ($_POST as $cle => $val)  $INPUT[$cle] = NormIN_gal ($cle); 
	debug (0x40, 'INPUT', $INPUT);
	return;
}	

#=============================================================== OuvrirBDD ===
function OuvrirBDD ($galerie) {
# Ouvrir la base de données	
	global $Bases, $dblink;

	//  connecter MySQL et ouvrir la base
	$gal_item = $galerie->attributes();
	$db = (string) $gal_item->db;				// Prendre le No de BdD associé au fichier

	$dbase = $Bases[$db];      

	if ($dblink = mysqli_connect ($dbase['serveur'], $dbase['user'], $dbase['passe'], $dbase['nom'])) { 
			debug (1, "mySQL session ouverte - Base de données " . $dbase['nom'] . " ouverte");

		// Spécifier le codage 
		mysqli_set_charset($dblink, "utf8");
			
	} else {
			die ("Erreur lors de l'ouverture - ". mysqli_connect_error ());
			// >>>>> EXIT abandon
	}
}

###################################################################### requete ###
function requete ($requete) {
# Effectue une requête
   global $dblink, $result;

   debug (2, "Requête", $requete);

   if ((false === ($result = mysqli_query ($dblink, $requete))) && (($err = mysqli_errno ($dblink)) != 1046))	
      debug (1, "Requête incorrecte : $requete <br>$err : " . mysqli_error ($dblink));
   return $result;	
}

###################################################################### Sauve_xml ###
function Sauve_xml($file) {
# Sauvegarde du fichier xml avant modification 
# Le nom du fichier est complété par un groupe date/heure
#
	global $dir_sauve, $dir_textes;
	if (!copy($dir_textes.$file.'.xml', $dir_sauve.$file.'_'.date('Ymd_B').'.xml')) 
		echo "La copie du fichier $file a échoué...\n";
}

###################################################################### Select_db ###
function Select_db () {

	// Traitement de l'entrée éventuelle
	$dbget = @$_GET['db'];
	unset ($_GET['db']); 		// on nettoie pour ne pas le traiter une fois dans "index" et une fois dans "galerie"
	if (isset ($dbg) AND !is_numeric($dbg)) 	DIE ("*** Paramètre 'db' faux ! ***"); 

	// Si la session n'est pas ouverte par "index", l'ouvrir maintenant pour lire le numéro de base
	// et éventuellement les droits d'accès si session ouverte par DBAconit
	if (session_id() == "") session_start ();		
	$db = @$_SESSION['db'];

	// Si l'URL inclut un paramètre db...
	if (isset ($dbget)) {
		// s'il n'est pas identique à db-session, accepter la nouvelle base mais effacer les droits d'accès éventuels
		if ($dbget != $db) {
			$_SESSION['db'] = $db = $dbget;
			$_SESSION ['statut'] = 'visiteur';
		}
	}
   return $db;	
}

######################################################################### Typo ###
function Typo ($str, $p=FALSE) {
#	Mise en forme typographique HTML 
#------------------------------------------------------------------------------
	$pattern = array ('#\n\n#', '#\n#', '#\{\{\{#', '#\{\{#', '#\{#', '#\}\}\}#', '#\}\}#', '#\}#',
										'#\ ?;#', '#\ ?:#',	'#\ ?!#', '#\ ?\?#', 
										'#«\ ?#', '#\ ?»#', '#_#',
										 '#\[\[#', '#\=\=#', '#\]\]#');
	$replacement = array ('</p><p>', '<br/>', '</p><h3>', '<b>', '<i>', '</h3><p>', '</b>', '</i>',
												'&thinsp;;', '&nbsp;:',	'&thinsp;!', '&thinsp;?',
												'«&thinsp;', '&thinsp;»', '&nbsp;',
												'<a href="http://', '" target="_blank">', '</a>' );
	if ($p) return  '<p>'. preg_replace ($pattern, $replacement, $str) .'</p>';
	else return preg_replace ($pattern, $replacement, $str);
}	

################################################################ XML_br ###
function XML_br ($loop, $attr, $evalue = '') {
	if ($loop !== null) return 'TAG, ACT';				// $Xaction = 'TAG, ACT' inchangé
	else return 'ACT';								// pas de recopie de la balise fermante
}

################################################################ XML_hr ###
function XML_hr ($loop, $attr, $evalue = '') {
	if ($loop !== null) return 'TAG, ACT';				// $Xaction = 'TAG, ACT' inchangé
	else return 'ACT';								// pas de recopie de la balise fermante
}

################################################################ XML_img ###
function XML_img ($loop, $attr, $evalue = '') {
	if ($loop !== null) return 'TAG, ACT';				// $Xaction = 'TAG, ACT' inchangé
	else return 'ACT';								// pas de recopie de la balise fermante
}

######################################################################
?>