<?php
#########################################################################################
# v8.1		190821	PhD		Cr�ation
# v9			190903	PhD		Compl�ter, tri des salles pour affichage �diteur
# v9.1		210122	PhD		'f_gal' remplac� par 'mod', 'ed_tab' devient 'ed_hall', synchro uniquement pour galeries
#########################################################################################


require_once ('./edit_inc.php');	
# Initialisations et ouverture de session
require_once ('./init.inc.php');	
# Envoyer l'en-t�te HTML (apr�s ouverture de session...)
require_once ('./include/inc_tete.php');
	
####### Traitement des entr�es
$fgal = $_SESSION['fgal'];
$file = $dir_textes.$fgal.'.xml';

# Toutes les entr�es POST sont trait�es d'un coup, dans le tableau INPUT
NormINPUT_gal ();
$action = $INPUT['action'] ?? 'annuler'; 

// V�rifier que le nom du fichier galerie existe.
if (!file_exists ($dir_textes.$fgal.'.xml')) $fgal = 'galerie0';	// Par d�faut galerie informatique...
$Xvars['fgal'] = $fgal;
$Xvars ['typ'] = substr ($fgal,0,3);	// ATTENTION : type "galerie" est d�termin� sur 3 caract�res du nom de fichier...

$Xvars['mod'] = 'ed_hall';						// traitement en cours

###################################################################################### Ouvrir le fichier XML galerie ###

$galerie = Xopen ($dir_textes.$fgal.'.xml');
// debug (255, 'GALERIE', $galerie);
	
#======================= Effectuer la modification====================================
switch ($action) {
	case 'annuler':
		break;
	
	case 'enregistrer':
		// Sauvegarde de la version d'origine
		Sauve_xml ($fgal);

		//Modifier attributs du hall
		$hall = $galerie->hall;
	
		$hall['modele'] = $INPUT['new_modele'];
		$hall['banniere'] = $INPUT['new_ban'];
		$hall['etat'] = $INPUT['new_etat'];

		// Modifier �l�ments du hall			
		$hall->nom = $INPUT['new_nom'];
		$hall->sujet = $INPUT['new_sujet'];
		$hall->difficulte = $INPUT['new_diff'];
		$hall->presentation = $INPUT['new_present'];

### Boucle de mise � jour des salles	
### sauf dans le cas d'une cr�ation de galerie/parcours. 
### Dans ce cas, au premier tour, les salles ne sont pas d�finies
		if (!empty ($INPUT['new_sal_nr'])) {
			$new_sal_nr = $INPUT['new_sal_nr'];
			$new_sal_nom = $INPUT['new_sal_nom'];
			$new_sal_etat = $INPUT['new_sal_etat'];
		
			$i = 0;
			foreach ($galerie->salle as $salle) {
				$salle['nr'] = $new_sal_nr[$i];
				$salle->nom = $new_sal_nom[$i];	
				$salle['etat'] = $new_sal_etat[$i];
				$i++;
			}
		}
		
### Cr�ation �ventuelle d'une salle suppl�mentaire
		if (!empty ($INPUT['add_nr'])) {
			$add_salle = $galerie->addChild ('salle');
			$add_salle->addAttribute ('nr', $INPUT['add_nr']);
			$add_salle->addAttribute ('modele', $INPUT['add_modele']);
			$add_salle->addAttribute ('etat', 'travaux');
			$add_salle->addChild ('nom', $INPUT['add_nom']);
	
			// Pr�parer les vitrines ou �tapes (suivant le mod�le demand�)
			$tab = trim (substr ($INPUT['add_modele'], 0, 3));
			if ($tab == 'sal') $add_salle->addAttribute ('deco', 'aa');
			else  $add_salle->addAttribute ('deco', 'pa');
			
			$elt = (substr ($INPUT['add_modele'], 0, 3) == 'sal') ? 'vitr_' : 'etape_';
			for ($i = 0; $i <= 9; $i++) {
				$add_salle->addChild ($elt.$i);
			}
		}

### Enregistrer la MODIFICATION
		$galerie->asXML($file); 
		$Xvars['enreg_OK'] = 'TRUE';
		
# 	Synchroniser les codes 'museevirtuel' dans la BdD s'il s'agit d'un fichier galerie
		if ($Xvars['typ'] == 'gal') Synchro ($galerie);
		
		break;
}

#########################################################################################

###  Prendre dans le hall les caract�ristiques de l'en t�te, 
	$hall = $galerie->hall;
	$Xvars['hall_modele'] = trim ((string) $hall['modele']);		
	if (isset($hall['banniere'])) $Xvars['hall_ban'] = trim ((string) $hall['banniere']) ;
	else $Xvars['hall_ban'] = $ban_defaut; // t�ti�re par d�faut
	$Xvars['hall_etat'] = (string) $hall['etat'];	

	$Xvars['hall_nom'] = (string) $hall->nom;
	$Xvars['hall_sujet'] = (string) $hall->sujet;
	$Xvars['hall_diff'] = trim ((string) $hall->difficulte);
	$Xvars['hall_present'] = (string) $hall->presentation;
	
	// Dossiers images
	$Xvars['dir_img'] = $dir_img;
	$Xvars['dir_img_banniere'] = $dir_img_banniere;

####### Dresser le tableau des salles
	$tab_salle = array ();
	$n = 0;

	foreach ($galerie->salle as $salle) {
		$tab_salle[$n]['nr'] = (string) $salle['nr']; 
		$tab_salle[$n]['etat'] = $etat =(string) $salle['etat'];
		$tab_salle[$n]['nom'] = (string) $salle->nom;
		$n++;
	}
	
	// Trier la table dans l'ordre des Num�ros de salles et transmettre le tableau
	sort ($tab_salle);
	$Xvars['tab_salle'] = $tab_salle;
	


#======================= Effectuer la mise en forme � partir du mod�le s�lectionn�
// Ouvrir le fichier ....
$hall_xml = Xopen ('./XML_modeles/edit_hall.xml');
Xpose ($hall_xml);


####### Affichage pied de page
####################################################################################################### Pied de page ###

$pied = Xopen ('./XML_modeles/pied_page.xml');		// Ouverture et contr�le 
Xpose ($pied);

# Sortie
Fin ();
?>